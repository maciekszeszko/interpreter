module DataTypes where

import qualified Data.List as List
import qualified Data.Map as Map
import qualified Data.Set as Set



-- *************** --
-- *             * --
-- * Typeclasses * --
-- *             * --
-- *************** --

--------------------------
-- Comparison operators --
--------------------------
class (Ord a, EqOp a) => OrdOp a where
  greater      :: a -> a -> BoolT
  greaterEqual :: a -> a -> BoolT
  lesser       :: a -> a -> BoolT
  lesserEqual  :: a -> a -> BoolT

class (Eq a) => EqOp a where
  equal    :: a -> a -> BoolT
  notEqual :: a -> a -> BoolT

  

---------------------------
-- Arithmetics operators --
---------------------------
class Arithmetics a where
  add :: a -> a -> a
  subtract :: a -> a -> a
  multiply :: a -> a -> a
  divide :: a -> a -> a


--------------------------
-- Structure operations --
--------------------------
class StructureUnionOp a where
  union        :: a -> a -> a

class StructureDifferenceOp a where
  difference :: a -> a -> a


-------------------------
-- Default BoolT Value --
-------------------------
class DefBoolTVal a where
  toBoolT :: a -> BoolT


class DefBoolVal a where
  toBool :: a -> Bool


class LogicOp a where
  and :: a -> a -> a
  or  :: a -> a -> a
  not :: a -> a


extractBoolVal :: BoolT -> Bool
extractBoolVal (BT boolT) = boolT



-- ********* --
-- *       * --
-- * BoolT * --
-- *       * --
-- ********* --
data BoolT    = BT Bool deriving  (Eq, Ord)

-- Default BoolT Value
instance DefBoolTVal BoolT where
  toBoolT = id

instance DefBoolVal BoolT where
  toBool = extractBoolVal . toBoolT

instance Show BoolT where
  show (BT bVal) = show bVal

instance LogicOp BoolT where
  or (BT bVal) (BT bVal')  = BT $ (||) bVal bVal'
  and (BT bVal) (BT bVal') = BT $ (&&) bVal bVal'
  not (BT bVal)            = BT $ Prelude.not bVal


-- ************ --
-- *          * --
-- * IntegerT * --
-- *          * --
-- ************ --
data IntegerT = IT Integer deriving (Eq, Ord)

-- Comparisions
instance EqOp IntegerT where
  equal (IT a) (IT b) = BT $ (==) a b
  notEqual a b        = BT $ (/=) a b

instance OrdOp IntegerT where
  greater (IT a) (IT b)      = BT $ (>) a b
  greaterEqual (IT a) (IT b) = BT $ (>=) a b
  lesser (IT a) (IT b)       = BT $ (<) a b
  lesserEqual (IT a) (IT b)  = BT $ (<=) a b


-- Arithmetics
instance Arithmetics IntegerT where
  add (IT a) (IT b)      = IT $ (+) a b
  subtract (IT a) (IT b) = IT $ (-) a b
  multiply (IT a) (IT b) = IT $ (*) a  b
  divide (IT a) (IT b)   = IT $ div a b


-- Default BoolT Value
instance DefBoolTVal IntegerT where
  toBoolT (IT 0) = BT $ False
  toBoolT _      = BT $ True


instance DefBoolVal IntegerT where
  toBool = extractBoolVal . toBoolT


instance Show IntegerT where
  show (IT iVal) = show iVal



-- ********* --
-- *       * --
-- * DictT * --
-- *       * --
-- ********* --
data DictT    = DT {
                  boolKeyDict :: Map.Map BoolT ValueT,
                  intKeyDict  :: Map.Map IntegerT ValueT
                } deriving (Eq)

-- Equal Comparisons
instance EqOp DictT where
  equal a b    = BT $ (==) a b
  notEqual a b = BT $ (/=) a b


-- Structure Operations
instance StructureUnionOp DictT where
  union (DT bkDict ikDict) (DT bkDict' ikDict')      =
    DT (Map.union bkDict bkDict') (Map.union ikDict ikDict')

instance StructureDifferenceOp DictT where
  difference (DT bkDict ikDict) (DT bkDict' ikDict') =
    DT (Map.difference bkDict bkDict') (Map.difference ikDict ikDict')


-- Default BoolT Value
instance DefBoolTVal DictT where
  toBoolT (DT bkDict ikDict) = BT $ Prelude.not $ (&&) (Map.null bkDict) (Map.null ikDict)


instance DefBoolVal DictT where
  toBool = extractBoolVal . toBoolT


showDict :: DictT -> String
showDict dict = "{" ++ showDict' dict ++ "}"


showDict' :: DictT -> String
showDict' (DT bkDict ikDict) =
  let bkDictList = Map.toList bkDict
      ikDictList = Map.toList ikDict
      isCommaSep = if (||) (null bkDictList) (null ikDictList) then "" else ", "
  in (showFromList bkDictList) ++ isCommaSep ++  (showFromList ikDictList)
                                 

transformToStrList :: (Show key) => [(key, ValueT)] -> [String]
transformToStrList = map (\(key, value) -> (show key) ++ ": " ++ (show value))

showFromList :: (Show key) => [(key, ValueT)] -> String
showFromList = List.intercalate ", " . transformToStrList


instance Show DictT where
  show dict = showDict dict


-- ********* --
-- *       * --
-- * ListT * --
-- *       * --
-- ********* --
data ListT    = LIT [ValueT] deriving (Eq)

-- Equal Comparisons
instance EqOp ListT where
  equal (LIT a) (LIT b)    = BT $ (==) a b
  notEqual (LIT a) (LIT b) = BT $ (/=) a b


-- Structure Operations
instance StructureUnionOp ListT where
  union (LIT a) (LIT b) = LIT $ (++) a b


-- Default Bool Value
instance DefBoolTVal ListT where
  toBoolT (LIT []) = BT $ False
  toBoolT _       = BT $ True


instance DefBoolVal ListT where
  toBool = extractBoolVal . toBoolT


instance Show ListT where
  show (LIT list) = "[" ++ List.intercalate ", " (map show list) ++ "]"



-- ********** --
-- *        * --
-- * TupleT * --
-- *        * --
-- ********** --
data TupleT   = TT [ValueT] deriving (Eq)

-- Equal Comparisons
instance EqOp TupleT where
  equal (TT a) (TT b)    = BT $ (==) a b
  notEqual (TT a) (TT b) = BT $ (/=) a b


-- Structure Operations
instance StructureUnionOp TupleT where
  union (TT a) (TT b) = TT $ (++) a b


-- Default Bool Value
instance DefBoolTVal TupleT where
  toBoolT (TT []) = BT $ False
  toBoolT _       = BT $ True


instance DefBoolVal TupleT where
  toBool = extractBoolVal . toBoolT
instance Show TupleT where
  show (TT tuple) = "<<" ++ List.intercalate ", " (map show tuple) ++ ">>"


data ValueT   = BoolObject    BoolT
              | IntegerObject IntegerT
              | DictObject    DictT
              | ListObject    ListT
              | TupleObject   TupleT
              | NoneObject    
               deriving (Eq)


instance Show ValueT where
  show valueT = case valueT of
                  (BoolObject boolT)       -> show boolT
                  (IntegerObject integerT) -> show integerT
                  (DictObject dictT)       -> show dictT
                  (ListObject listT)       -> show listT
                  (TupleObject tupleT)     -> show tupleT
                  NoneObject               -> "undefined"


instance DefBoolVal ValueT where
  toBool valueT = case valueT of
                    (BoolObject boolT)       -> toBool boolT
                    (IntegerObject integerT) -> toBool integerT
                    (DictObject dictT)       -> toBool dictT
                    (ListObject listT)       -> toBool listT
                    (TupleObject tupleT)     -> toBool tupleT
                    NoneObject               -> False
 
                  
instance DefBoolTVal ValueT where
  toBoolT valueT = case valueT of
                     (BoolObject boolT)       -> toBoolT boolT
                     (IntegerObject integerT) -> toBoolT integerT
                     (DictObject dictT)       -> toBoolT dictT
                     (ListObject listT)       -> toBoolT listT
                     (TupleObject tupleT)     -> toBoolT tupleT
                     NoneObject               -> BT False
