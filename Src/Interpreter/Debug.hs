module Debug where

import qualified Data.Map as Map
import qualified Data.Set as Set

import DataTypes
import MainEnvironment

-----------
-- Debug --
-----------
printVarMap :: (Show a) => (a, ValueT) -> IO ()
printVarMap (key, value) =
  putStrLn $ "Key: " ++ (show key) ++ ", value: " ++ (show value) ++ "."


printVarSet :: VarName -> IO ()
printVarSet elem =
  putStrLn $ "Elem: " ++ (show elem) ++ "."


printVarEnv :: MainEnv -> String -> IO ()
printVarEnv (MEnv _ _ varEnv _ _) desc =
  do let gVariables      = Map.toList (globalVariablesVEnv varEnv)
         gVariablesInUse = Set.toList (globalVariablesInUseVEnv varEnv)
         lVariables      = Map.toList (localVariablesVEnv varEnv)
     putStrLn $ "+++++++++++++++++++++++++"
     putStrLn $ desc
     putStrLn $ "+++++++++++++++++++++++++"
     putStrLn $ "Global Vars: "
     mapM_ printVarMap gVariables
     putStrLn $ "-----------------------------"
     putStrLn $ "Global In Use: "
     mapM_ printVarSet gVariablesInUse
     putStrLn $ "-----------------------------"
     putStrLn $ "Local Vars: "
     mapM_ printVarMap lVariables

