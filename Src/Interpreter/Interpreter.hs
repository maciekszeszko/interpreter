module Interpreter where

import Control.Monad.Error
import Control.Monad.IO.Class

import qualified Data.List as List
import qualified Data.Map as Map
import qualified Data.Set as Set

import Data.Maybe


import AbsGrammar

import DataTypes
import Debug
import InterpreterMessages
import MainEnvironment


emptyEnv :: MainEnv
emptyEnv = let emptyFunEnv = FEnv Map.empty
               emptyVarEnv = VEnv Map.empty Set.empty Map.empty
               initRetEnv  = REnv NO_RETURN NoneObject
           in MEnv Nothing emptyFunEnv emptyVarEnv initRetEnv Global
         
       
type Result     = ()
type EvalProg a = ErrorT String IO a

-- Note: It's crucial to notice that `return` stmt is not as trivial as one might think.
--       It can be invoked both in nested stmts as well as in trivial function body.
--       Sometimes, it may also occur even when unecessary.
--       So all those cases should be considered.
--
--       Core assumption:
--          'Unassigned function expression callback is forbidden to return a value.'
--
--       The reason is pretty obvious - if it's not assigned to anywhere, so why why return stmt has been invoked?
--       It seems to be bad design and in fact, it is considered as a programmer fault there.
--
--       When are they used:
--         1) Generally in bodyList stmt. Notice that it can be nested, like:
--
--               forEach item in collection::
--                  |
--                  |
--                  if cond::
--                     |
--                     return 22
--                     |
--                     ..
--                  @
--                  |
--                  ..
--               @
--
--            It's naturally cascaded to the first outer function call.
--            After Static Analysis stage we are guaranteed that such exists,
--            because following the explanation above, we do not allow return stmts
--            in global scope.
-- 
--         2) callback()  # it could be 1) for: some_variable = callback()
 
data ExpectedReturnMode = RETURN_CASCADE                   -- 1)
                        | RETURN_FORBIDDEN                 -- 2)


runEvalProg :: EvalProg a -> IO (Either String a)
runEvalProg prog = runErrorT prog



-- **************************** --
-- *                          * --
-- * Manage Return Expression * --
-- *                          * --
-- **************************** --
getReturnExprState :: MainEnv -> ReturnExprState
getReturnExprState env = state $ retEnv env


updateReturnExprEnv :: MainEnv -> ReturnExprState -> ValueT -> MainEnv
updateReturnExprEnv (MEnv currFunName funEnv varEnv _ scope) returnExprState value =
  MEnv currFunName funEnv varEnv (REnv returnExprState value) scope


-- Note: By getting return expression value You reset `ReturnEnvironment`.
--       It's like for specfifc register types, when value is read register ic cleared.
getReturnExprValue :: MainEnv -> (ValueT, MainEnv)
getReturnExprValue env =
  let (MEnv currFunName funEnv varEnv retEnv scope) = env
      retVal = value retEnv
  in (retVal, updateReturnExprEnv env NO_RETURN retVal)


-- Note: Should be used in `evalFuncExpr()` and probably only in there.
checkExpectedReturnExprMode :: MainEnv -> ExpectedReturnMode -> EvalProg Result
checkExpectedReturnExprMode env expectedState =
  do let retState = getReturnExprState env
     case expectedState of
       RETURN_CASCADE   -> case retState of
                             NO_RETURN -> throwError $ functionNoReturnValueError $ fromJust $ currFunName env
                             UNHANDLED -> return $ ()
       RETURN_FORBIDDEN -> case retState of
                             NO_RETURN -> return $ ()
                             UNHANDLED -> throwError $ functionReturnValueError $ fromJust $ currFunName env



-- *********** --
-- *         * --
-- * Program * --
-- *         * --
-- *********** --
evalProg :: MainEnv -> Program -> EvalProg Result
evalProg env (Prog [])       = return $ ()
evalProg env (Prog (x : xs)) =
  case x of
                             -- Note: Thanks to the Static Analysis stage,
                             --       we are guaranteed that there are
                             --       no return stmt's in global scope.
                             --       Hence, no additional checks are required.  
    ProgStmt stmt         -> do env' <- evalStmt env stmt
                                let env'' = restoreGlobalScope env'
                                evalProg env'' (Prog xs)
    ProgFuncExpr funcExpr -> do (evExpr, env') <- evalFuncExpr env funcExpr RETURN_FORBIDDEN
                                let env'' = restoreGlobalScope env'
                                evalProg env'' (Prog xs)


restoreGlobalScope :: MainEnv -> MainEnv
restoreGlobalScope (MEnv currFunName funEnv varEnv retEnv _) =
  (MEnv currFunName funEnv varEnv retEnv Global)



-- ******** --
-- *      * --
-- * Stmt * --
-- *      * --
-- ******** --
evalStmt :: MainEnv -> Stmt -> EvalProg MainEnv
evalStmt env stmt =
  case stmt of
    PrintStmt printList                            -> evalPrintStmt env printList
    AssignStmt identList exprList                  -> evalAssignStmt env identList exprList
    FuncStmt declStmt bodyList                     -> evalFuncStmt env declStmt bodyList
    CondStmtBasic ifStmt elifStmtList              -> evalCondStmtBasic env ifStmt elifStmtList
    CondStmtComplexed ifStmt elifStmtList elseStmt -> evalCondStmtComplexed env ifStmt elifStmtList elseStmt
    ForeachLoopStmt iter collection bodyList       -> evalForeachLoopStmt env iter collection bodyList
    WhileLoopStmt expr bodyList                    -> evalWhileLoopStmt env expr bodyList
    StructureOpStmt structureOpStmt                -> evalStructureOpStmt env structureOpStmt



-- ************** --
-- *            * --
-- * Print Stmt * --
-- *            * --
-- ************** --
evalPrintStmt :: MainEnv -> [PrintExpr] -> EvalProg MainEnv
evalPrintStmt env []                   = return $ env
evalPrintStmt env ((PrintExpr x) : xs) =
  -- Note:  Extracting environment here is not a reduntant or routine stuff.
  --        One can imagine that during expression evaluation, program variable's
  --        environment can change because of some actions invoked by the func calls.
  do (evExpr, env') <- evalExpr env x
     printEvExpr evExpr
     evalPrintStmt env' xs


printEvExpr :: (MonadIO m) => ValueT -> m ()
printEvExpr value = liftIO $ putStrLn $ show value



-- *************** --
-- *             * --
-- * Assign Stmt * --
-- *             * --
-- *************** --
evalAssignStmt :: MainEnv -> [AssignIdent] -> [AssignExpr] -> EvalProg MainEnv
evalAssignStmt env [] [] = return $ env
evalAssignStmt env (x : xs) ((AssignExpr y) : ys) =
  case x of
    (AssignIdent (Ident ident))              ->
       do (evExpr, env')   <- evalExpr env y
          evalAssignStmt (addVariableToEnv env' ident evExpr) xs ys
    (AssignListElemIdent (Ident ident) expr) ->
       do (evExpr, env')   <- evalExpr env expr
          (evExpr', env'') <- evalExpr env' y
          env''' <- handleAssignListElemIdent env'' ident evExpr evExpr'
          evalAssignStmt env''' xs ys


addVariableToEnv :: MainEnv -> VarName -> ValueT -> MainEnv
addVariableToEnv env varName value =
  do let scope = getVariableScope env varName
     case scope of
       Global -> updateGlobalVariablesEnv env varName value
       Local  -> updateLocalVariablesMap env varName value



-- ************* --
-- *           * --
-- * Func Stmt * --
-- *           * --
-- ************* --
-- Note: This step actually does not require monad because there is now way to fail itself.
--       Hypothetically, pessimistic case is when invalid key type is detected
--       somewhere during evaluation of default parameter expression.
--       However, this cannot happen at that stage due to designed logic flow.
--       So monad is basically used there just to keep consistency with `evalStmt` call.
evalFuncStmt :: MainEnv -> FuncStmtDecl -> [GenericStmtBody] -> EvalProg MainEnv
evalFuncStmt env declStmt bodyList=
  do let (funName, funParams, defArgMapper) = extractFunHeader declStmt
         (MEnv currFunName funEnv varEnv retEnv scope) = env
         funExecute = FExecute bodyList defArgMapper
         varScope   = prepareVarScope env
         funDesc'   = Map.insert funName (FEDesc funExecute funParams varScope) (funDesc funEnv)
         funEnv'    = FEnv funDesc'
     return $ MEnv currFunName funEnv' varEnv retEnv scope


extractFunHeader :: FuncStmtDecl -> (FunName, FunParams, DefArgMapper)
extractFunHeader (FuncStmtDecl (FuncStmtDeclIdent (Ident ident)) funArgs) =
  let (funParams, defArgMapper) = extractFunParams funArgs
  in (ident, funParams, defArgMapper)


extractFunParams :: [FuncStmtArg] -> (FunParams, DefArgMapper)
extractFunParams funArgs = extractFunParams' funArgs (FParams Set.empty Map.empty [] Set.empty) Map.empty


-- Note: Extracting function parameters can be thought of like kind of preprocessing stage.
--       Because function default parameter can be also an expression then in particular
--       it is also legal to represent it as a function expression.
--       However, function expression may change program environment,
--       so that we have to guarantee the evaluation of default arguments
--       will be performed only when necessary. True evaluation of default function
--       parameters should take place in `evalFuncCallback`.
extractFunParams' :: [FuncStmtArg] -> FunParams -> DefArgMapper -> (FunParams, DefArgMapper)
extractFunParams' [] funParams defArgMapper                                      = (funParams, defArgMapper)
extractFunParams' (x : xs) (FParams defSet defMap orderList posSet) defArgMapper =
  case x of
    (FuncStmtDeclArgStrict (Ident ident))       ->
       let posSet'    = Set.insert ident posSet
           orderList' = orderList ++ [ident] 
           funParams' = FParams defSet defMap orderList' posSet'
       in extractFunParams' xs funParams' defArgMapper 
    (FuncStmtDeclArgDefault (Ident ident) expr) ->
       let defSet'    = Set.insert ident defSet
           orderList' = orderList ++ [ident] 
           funParams' = FParams defSet' defMap orderList' posSet
       in extractFunParams' xs funParams' (Map.insert ident expr defArgMapper)


prepareVarScope :: MainEnv -> VarScope
prepareVarScope (MEnv currFunName funEnv varEnv return scope) =
  let gVariablesVScope      = globalVariablesInUseVEnv varEnv
      lVariablesVScope      = Set.fromList $ Map.keys $ localVariablesVEnv varEnv
  in VScope gVariablesVScope lVariablesVScope
   


-- ********************* --
-- *                   * --
-- * Generic Body Stmt * --
-- *                   * --
-- ********************* --
-- Note_1: Keep in mind that there can exist function arguments that might overlap non-globals!

evalBodyList :: MainEnv -> [GenericStmtBody] -> EvalProg MainEnv
evalBodyList env bodyList = do env' <- evalBodyList' env bodyList
                               let env'' = prepareResponseEnv env env'
                                   env''' = unionEnvironments env env''  -- TODO: Czy to jest w ogóle potrzebne tutaj?-}
                               return $ env'''
                             

evalBodyList' :: MainEnv -> [GenericStmtBody] -> EvalProg MainEnv
evalBodyList' env []       = return $ env
evalBodyList' env (x : xs) =
  case x of
    GenericStmtBodyStmt stmt         ->
      do env' <- evalStmt env stmt
         let retState = getReturnExprState env'
         case retState of
           -- Cascade return value to outer scope until function expression callable will be reached.
           UNHANDLED -> return $ env'
           NO_RETURN -> evalBodyList' env' xs
    GenericStmtBodyGlobalIdentStmt (GlobalIdentStmt (Ident ident)) ->
      do let env' = addVariableToGlobalScope env ident
         evalBodyList' env' xs
    GenericStmtBodyFuncExpr funcExpr ->
      do (_, env')  <- evalFuncExpr env funcExpr RETURN_FORBIDDEN
         evalBodyList' env' xs
    GenericStmtBodyReturn expr       ->
      do (evExpr, env') <- evalExpr env expr
         return $ updateReturnExprEnv env' UNHANDLED evExpr


addVariableToGlobalScope :: MainEnv -> VarName -> MainEnv
addVariableToGlobalScope env varName =
  let (MEnv currFunName funEnv varEnv retEnv scope) = env
      gVariablesInUse' = Set.insert varName (globalVariablesInUseVEnv varEnv)
      gVariables       = globalVariablesVEnv varEnv
      lVariables       = localVariablesVEnv varEnv
      varEnv'          = VEnv gVariables gVariablesInUse' lVariables
  in MEnv currFunName funEnv varEnv' retEnv scope


-- ********************* --
-- *                   * --
-- * Environment Union * --
-- *                   * --
-- ********************* --
prepareResponseEnv :: MainEnv -> MainEnv -> MainEnv
prepareResponseEnv env env' =
  case (scope env') of
    Local  -> prepareResponseEnvLocalScope env env'
    Global -> prepareResponseEnvGlobalScope env env'


prepareResponseEnvGlobalScope :: MainEnv -> MainEnv -> MainEnv
prepareResponseEnvGlobalScope env env' =
  let (MEnv currFunName funEnv varEnv _ scope) = env
      (MEnv _ _ varEnv' retEnv' _)             = env'
      gVariablesSet   = Set.fromList $ Map.keys $ globalVariablesVEnv varEnv
      gVariables      = prepareSubMap gVariablesSet $ globalVariablesVEnv varEnv'
      gVariablesInUse = globalVariablesInUseVEnv varEnv
      lVariables      = Map.empty
      varEnv''        = VEnv gVariables gVariablesInUse lVariables

  -- Note: `retEnv` does not really matter there because it cannot be returned from global scope.
  in MEnv currFunName funEnv varEnv'' retEnv' scope
      

prepareResponseEnvLocalScope :: MainEnv -> MainEnv -> MainEnv
prepareResponseEnvLocalScope env env' =
  let (MEnv currFunName funEnv varEnv' retEnv scope) = env'
      currFunEnv          = fromJust $ Map.lookup (fromJust $ currFunName) (funDesc funEnv)
      lVariablesVScopeSet = localVariablesVScope $ varScope currFunEnv
      gVariablesVScopeSet = globalVariablesVScope $ varScope currFunEnv
      funParamNames       = Set.fromList $ orderList $ funParams currFunEnv

      -- Handle variables overlapping phenomenon. 
      lVariablesSet       = Set.difference lVariablesVScopeSet funParamNames
      lVariables          = prepareSubMap lVariablesSet (localVariablesVEnv varEnv')

      -- Note: It's not required to filter the globals here.
      --       The place where the function call has been invoked has for sure
      --       at least the same global variables visibility scope as the callback.
      --       Thanks to the successful pass of Static Analysis stage,
      --       it is certain the callback can only refer to the globals defined 'above'.
      --       Hence, there is no chance the callback modified other globals defined 'below'
      --       in the code, which results in ability of basically passing global variables
      --       structure without any uncertainty.
      --       What is more, notice that changing value of global variable there
      --       even if it's invisible from callable scope, is perfectly fine operation.
      --
     
      gVariablesInUseVEnv = globalVariablesInUseVEnv (varEnv env)
      varEnv''            = VEnv (globalVariablesVEnv varEnv') gVariablesInUseVEnv lVariables
  in MEnv currFunName funEnv varEnv'' retEnv scope


prepareSubMap :: VarSet -> VarMap -> VarMap
prepareSubMap varSet varMap = Map.filterWithKey (\key _ -> Set.member key varSet) varMap


-- Note: Short explanation about what those two environments really are.
--       The former one is initial environment given just before evaluation
--       of body list stmts. The latter is environment computed during
--       evaluation of the body list which domain(variable map names)
--       is cut with domain of the initial environment.
--       Hence, the second env contains modified 
unionEnvironments :: MainEnv -> MainEnv -> MainEnv
unionEnvironments env env' =
  let (MEnv currFunName funEnv varEnv _ scope) = env
      (MEnv _ _ varEnv' retEnv' _) = env'

      -- Update Global Map
      gVariablesVEnv = unionEnvironments' globalVariablesVEnv varEnv varEnv'

      -- Update Local Map
      lVariablesVEnv = unionEnvironments' localVariablesVEnv varEnv varEnv'

      -- Restore Global Variables In Use
      gVariablesInUseVEnv = globalVariablesInUseVEnv varEnv
      
      varEnv'' = VEnv gVariablesVEnv gVariablesInUseVEnv lVariablesVEnv 
  in MEnv currFunName funEnv varEnv'' retEnv' scope


unionEnvironments' :: (VarEnv -> VarMap) -> VarEnv -> VarEnv -> VarMap
unionEnvironments' f varEnv varEnv' = Map.union (f varEnv') (f varEnv)



-- ******************* --
-- *                 * --
-- * Cond Stmt Basic * --
-- *                 * --
-- ******************* --
evalCondStmtBasic :: MainEnv -> CondStmtIf -> [CondStmtElif] -> EvalProg MainEnv
evalCondStmtBasic env ifStmt elifStmtList =
  do (cond, env') <- evalCondStmtIf env ifStmt
     if Prelude.not cond then
        do (cond', env'') <- evalCondStmtElifList env elifStmtList
           return $ env''
        else
           return $ env'


evalCondStmtComplexed :: MainEnv -> CondStmtIf -> [CondStmtElif] -> CondStmtElse -> EvalProg MainEnv
evalCondStmtComplexed env ifStmt elifStmtList elseStmt =
  do (cond, env') <- evalCondStmtIf env ifStmt
     if Prelude.not cond then
        do (cond', env'') <- evalCondStmtElifList env elifStmtList
           if Prelude.not cond' then
              evalCondStmtElse env elseStmt
              else
                 return $ env''
        else
           return $ env'



-- *************** --
-- *             * --
-- * Cond Stmt's * --
-- *             * --
-- *************** --
evalCondStmt :: MainEnv -> Expr -> [GenericStmtBody] -> EvalProg (Bool, MainEnv)
evalCondStmt env expr bodyList =
  do (evExpr, env') <- evalExpr env expr
     let cond = toBool evExpr
     if cond then
        do env'' <- evalBodyList' env' bodyList
           return $ (cond, env'')
        else
           return $ (cond, env')

  
evalCondStmtIf :: MainEnv -> CondStmtIf -> EvalProg (Bool, MainEnv)
evalCondStmtIf env (CondStmtIf expr bodyList) = evalCondStmt env expr bodyList


evalCondStmtElifList :: MainEnv -> [CondStmtElif] -> EvalProg (Bool, MainEnv)
evalCondStmtElifList env elifStmtList = evalCondStmtElifList' env False elifStmtList


evalCondStmtElifList' :: MainEnv -> Bool -> [CondStmtElif] -> EvalProg (Bool, MainEnv)
evalCondStmtElifList' env False []    = return $ (False, env)
evalCondStmtElifList' env True _      = return $ (True, env)
evalCondStmtElifList' env False
  ((CondStmtElif expr bodyList) : xs) = 
     do (cond, env') <- evalCondStmt env expr bodyList
        evalCondStmtElifList' env' cond xs


evalCondStmtElse :: MainEnv -> CondStmtElse -> EvalProg MainEnv
evalCondStmtElse env (CondStmtElse bodyList) = evalBodyList env bodyList



-- ********************* --
-- *                   * --
-- * Foreach Stmt Loop * --
-- *                   * --
-- ********************* --
-- Note: Collection is immutable when iterated over.
evalForeachLoopStmt :: MainEnv -> Ident -> Ident -> [GenericStmtBody] -> EvalProg MainEnv
evalForeachLoopStmt env (Ident iter) (Ident collectionIdent) bodyList =
  do let primitiveIterInfo = isVariablePresent env iter
     collectionList <- transformCollection env collectionIdent
     env' <- evalForeachLoopStmt' env iter collectionList bodyList
     return $ restoreOldIterVarContents env' iter primitiveIterInfo
     

evalForeachLoopStmt' :: MainEnv -> VarName -> [ValueT] -> [GenericStmtBody] -> EvalProg MainEnv
evalForeachLoopStmt' env iter [] _             = return $ env
evalForeachLoopStmt' env iter (x : xs) bodyList =
  do let env' = addVariableToEnv env iter x
     env'' <- evalBodyList' env' bodyList
     let retState = getReturnExprState env''
     case retState of
       UNHANDLED -> return $ env''
       NO_RETURN -> evalForeachLoopStmt' env'' iter xs bodyList


restoreOldIterVarContents :: MainEnv -> VarName -> Maybe (Scope, ValueT) -> MainEnv
restoreOldIterVarContents env iter primitiveIterInfo =
  if isNothing primitiveIterInfo then
     deleteIterVar env iter
     else
        replaceNewIterVarWithOldOne env iter primitiveIterInfo
          

deleteIterVar :: MainEnv -> VarName -> MainEnv
deleteIterVar (MEnv currFunName funEnv varEnv retEnv scope) iter =
  let gVariables      = globalVariablesVEnv varEnv
      gVariablesInUse = globalVariablesInUseVEnv varEnv
      lVariables      = localVariablesVEnv varEnv
  in case scope of
       Local  -> let lVariables' = Map.delete iter lVariables
                     varEnv'     = VEnv gVariables gVariablesInUse lVariables'
                 in MEnv currFunName funEnv varEnv' retEnv scope
       Global -> let gVariables' = Map.delete iter gVariables
                     varEnv'     = VEnv gVariables' gVariablesInUse lVariables
                 in MEnv currFunName funEnv varEnv' retEnv scope
                 

replaceNewIterVarWithOldOne :: MainEnv -> VarName -> Maybe (Scope, ValueT) -> MainEnv
replaceNewIterVarWithOldOne (MEnv currFunName funEnv varEnv retEnv scope) iter primitiveIterInfo =
  let gVariables      = globalVariablesVEnv varEnv
      gVariablesInUse = globalVariablesInUseVEnv varEnv
      lVariables      = localVariablesVEnv varEnv
      (varScope, val) = fromJust $ primitiveIterInfo
  in case varScope of
       Local  -> let lVariables' = Map.insert iter val lVariables
                     varEnv'     = VEnv gVariables gVariablesInUse lVariables
                 in MEnv currFunName funEnv varEnv' retEnv scope
       Global -> let gVariables' = Map.insert iter val gVariables
                     varEnv'     = VEnv gVariables' gVariablesInUse lVariables
                 in MEnv currFunName funEnv varEnv' retEnv scope


isVariablePresent :: MainEnv -> VarName -> Maybe (Scope, ValueT)
isVariablePresent (MEnv currFunName funEnv varEnv retEnv scope) varName =
  let gVariable = Map.lookup varName (globalVariablesVEnv varEnv)
      lVariable = Map.lookup varName (localVariablesVEnv varEnv)
  in if (||) (Set.member varName (globalVariablesInUseVEnv varEnv)) ((==) scope Global) then
        Just (Global, fromJust $ gVariable)
        else if isJust lVariable then
           Just (Local, fromJust $ lVariable)
           else
              Nothing
 


-- ************************** --
-- *                        * --
-- * Collection Transformer * --
-- *                        * --
-- ************************** --
-- Note: There is no need of having monadic wrapper itself because all
--       the data is computed and stored as concrete ValueT objects.
--       In other words, it does not rely on any non-computed variables.
--       However, non-iterable variable can be delivered which will cause error.
--       Monadic type is only for consistency purposes there.
transformCollection :: MainEnv -> VarName -> EvalProg [ValueT]
transformCollection env collectionName =
  do let collection = snd $ fromJust $ isVariablePresent env collectionName
     isCollection collection collectionName
     case collection of
       (DictObject dObject)     -> transformDictCollection env dObject
       (ListObject (LIT list))  -> return $ list
       (TupleObject (TT tuple)) -> return $ tuple


transformDictCollection :: MainEnv -> DictT -> EvalProg [ValueT]
transformDictCollection env (DT bkDict ikDict) =
  do let transformedBKDict = transformBoolDictCollection' env bkDict
         transformedIKDict = transformIntegerDictCollection' env ikDict
     return $ (++) transformedBKDict transformedIKDict


---------------------------------------
-- Transform Bool dict collection --
---------------------------------------
transformBoolDictCollection' :: MainEnv -> BoolMap -> [ValueT]
transformBoolDictCollection' env collection = map transformBoolDictF (Map.toList collection)


transformBoolDictF :: (BoolT, ValueT) -> ValueT
transformBoolDictF (key, value) = DictObject (DT (Map.singleton key value) Map.empty)


---------------------------------------
-- Transform Integer dict collection --
---------------------------------------
transformIntegerDictCollection' :: MainEnv -> IntegerMap -> [ValueT]
transformIntegerDictCollection' env collection = map transformIntegerDictF (Map.toList collection)


transformIntegerDictF :: (IntegerT, ValueT) -> ValueT
transformIntegerDictF (key, value) = DictObject (DT Map.empty (Map.singleton key value))


isCollection :: ValueT -> VarName -> EvalProg Result
isCollection value collectionName =
  case value of
    (DictObject dObject)  -> return $ ()
    (ListObject lObject)  -> return $ ()
    (TupleObject tObject) -> return $ ()
    otherwise             -> throwError $ notIterableCollectionError $ collectionName



-- ******************* --
-- *                 * --
-- * While Loop Stmt * --
-- *                 * --
-- ******************* --
evalWhileLoopStmt :: MainEnv -> Expr -> [GenericStmtBody] -> EvalProg MainEnv
evalWhileLoopStmt env expr bodyList =
  do (evExpr, env') <- evalExpr env expr
     let cond = toBool evExpr
     if cond then
        do env'' <- evalBodyList' env' bodyList
           let retState = getReturnExprState env''
           case retState of
             UNHANDLED -> return $ env''
             NO_RETURN -> evalWhileLoopStmt env'' expr bodyList 
        else
           return $ env'



-- ********************* -- 
-- *                   * --
-- * Structure Op Stmt * --
-- *                   * --
-- ********************* --
evalStructureOpStmt :: MainEnv -> StructureOpStmt -> EvalProg MainEnv
evalStructureOpStmt env structOp =
  case structOp of
    (DictStructureOpStmt dictOpStmt)   -> evalDictOpStmt env dictOpStmt
    (ListStructureOpStmt listOpStmt)   -> evalListOpStmt env listOpStmt
    (TupleStructureOpStmt tupleOpStmt) -> evalTupleOpStmt env tupleOpStmt


evalDictOpStmt :: MainEnv -> DictOpStmt -> EvalProg MainEnv
evalDictOpStmt env dictOpStmt =
  case dictOpStmt of
    (DictUnionStmt (Ident ident) expr)         ->
       do (evExpr, env') <- evalExpr env expr
          handleDictUnionOpStmt env' ident evExpr
    (DictRemoveElemByKeyStmt (Ident ident) expr) ->
       do (evExpr, env') <- evalExpr env expr
          (value, env'') <- handleDictPopElemByKey env' ident evExpr
          return $ env''


evalListOpStmt :: MainEnv -> ListOpStmt -> EvalProg MainEnv
evalListOpStmt env listOpStmt =
  case listOpStmt of
    (ListPushElemStmt (Ident ident) expr) ->
       do (evExpr, env') <- evalExpr env expr
          handleListPushElem env' ident evExpr


evalTupleOpStmt :: MainEnv -> TupleOpStmt -> EvalProg MainEnv
evalTupleOpStmt env tupleOpStmt =
  case tupleOpStmt of
    (TupleReplaceElemAtPosStmt (Ident ident) expr expr') ->
       do (evExpr, env')    <- evalExpr env expr
          (evExpr', env'')  <- evalExpr env' expr'
          handleTupleReplaceElemAtPosOpStmt env'' ident evExpr evExpr'



-- ********************************** --
-- *                                * --
-- * Eval Structure Operation Stmts * --
-- *                                * --
-- ********************************** --
----------
-- List --
----------
handleAssignListElemIdent :: MainEnv -> VarName -> ValueT -> ValueT -> EvalProg MainEnv
handleAssignListElemIdent env varName index value =
  case index of
    (IntegerObject (IT iVal)) ->
       do let (evValue, varScope) = evalVariableValue env varName
          case evValue of
            (ListObject (LIT list)) -> do listRateRequestedRange varName list iVal iVal
                                          let newListContents = listReplaceElemAtIndex list iVal value
                                              newList         = ListObject (LIT newListContents)
                                          return $ updateEnvWithStructureOpResult env varName newList varScope
            otherwise               -> throwError $ notAssignableListCollectionError $ varName
    otherwise               -> throwError $ notIntegerIndexInCollectionError index varName


handleListPushElem :: MainEnv -> VarName -> ValueT -> EvalProg MainEnv
handleListPushElem env varName value =
  do let (evValue, varScope) = evalVariableValue env varName
     case evValue of
       (ListObject list) -> do let newListContents = union list (LIT [value])
                                   newList         = ListObject newListContents
                               return $ updateEnvWithStructureOpResult env varName newList varScope
       otherwise         -> throwError $ undefinedPushOperationError varName

                               
----------
-- Dict --
----------
handleDictUnionOpStmt :: MainEnv -> VarName -> ValueT -> EvalProg MainEnv
handleDictUnionOpStmt env varName value =
  case value of
    (DictObject dObject) ->
       do let (evValue, varScope) = evalVariableValue env varName
          case evValue of
            (DictObject dObject') -> do let newDictContents = union dObject dObject'
                                            newDict         = DictObject newDictContents
                                        return $ updateEnvWithStructureOpResult env varName newDict varScope
            otherwise               -> throwError $ invalidDictUnionOperandError varName
    otherwise               -> throwError $ invalidDictUnionExpressionError value


handleDictRemoveElemByKeyStmt :: MainEnv -> VarName -> ValueT -> EvalProg MainEnv
handleDictRemoveElemByKeyStmt env varName key =
  do (_, env') <- handleDictPopElemByKey env varName key
     return $ env'


-----------
-- Tuple --
-----------
handleTupleReplaceElemAtPosOpStmt :: MainEnv -> VarName -> ValueT -> ValueT -> EvalProg MainEnv
handleTupleReplaceElemAtPosOpStmt env varName index value =
  case index of
    (IntegerObject (IT iVal)) ->
       do let (evValue, varScope) = evalVariableValue env varName
          case evValue of
            (TupleObject (TT tuple)) -> do listRateRequestedRange varName tuple iVal iVal
                                           let newTupleContents = listReplaceElemAtIndex tuple iVal value
                                               newTuple         = TupleObject (TT newTupleContents)
                                           return $ updateEnvWithStructureOpResult env varName newTuple varScope
            otherwise                -> throwError $ invalidTupleReplaceAtOperandError varName
    otherwise               -> throwError $ notIntegerIndexInCollectionError index varName



-- ********************************************** --
-- *                                            * --
-- * Common structure schema operations support * --
-- *                                            * --
-- ********************************************** --
---------------
-- Dict Impl --
---------------
dictRateRequestedKeyExists :: DictT -> ValueT -> EvalProg ValueT
dictRateRequestedKeyExists dict key =
  let bDict = boolKeyDict dict
      iDict = intKeyDict dict
  in case key of
       (BoolObject bObject)    -> dictRateRequestedBoolKeyExists bDict iDict bObject
       (IntegerObject iObject) -> dictRateRequestedIntegerKeyExists bDict iDict iObject
       otherwise               -> throwError $ invalidKeyTypeForDictError key


type BoolMap    = Map.Map BoolT ValueT
type IntegerMap = Map.Map IntegerT ValueT
dictRateRequestedBoolKeyExists :: BoolMap -> IntegerMap -> BoolT -> EvalProg ValueT --(ValueT, ValueT)
dictRateRequestedBoolKeyExists bDict iDict bKey =
  do let value = Map.lookup bKey bDict
     case value of
       Just val -> return $ val
       Nothing  -> throwError $ boolKeyDoesNotExistError bKey


dictRateRequestedIntegerKeyExists :: BoolMap -> IntegerMap -> IntegerT -> EvalProg ValueT --(ValueT, ValueT)
dictRateRequestedIntegerKeyExists bDict iDict iKey =
  do let value = Map.lookup iKey iDict
     case value of
       Just val -> return $ val
       Nothing  -> throwError $ integerKeyDoesNotExistError iKey


-- Note: It is guaranteed that key is of valid type and that it exists
--       in the appropriate dictionary map.
dictDeleteKey :: DictT -> ValueT -> ValueT
dictDeleteKey (DT bKeyMap iKeyMap) key =
  case key of
    (BoolObject bObject)    -> let bKeyMap' = Map.delete bObject bKeyMap
                               in DictObject (DT bKeyMap' iKeyMap)
    (IntegerObject iObject) -> let iKeyMap' = Map.delete iObject iKeyMap
                               in DictObject (DT bKeyMap iKeyMap')


-----------------------
-- List & Tuple Impl --
-----------------------
listRateRequestedRange :: VarName -> [ValueT] -> Integer -> Integer -> EvalProg Result
listRateRequestedRange varName list fromR toR =
    
  do let fromR' = fromIntegral $ fromR
         toR'   = fromIntegral $ toR
     if fromR' > toR' then
        throwError $ invalidSublistRangeError fromR' toR'
        else
           if (>=) toR' (length list) then
              throwError $ indexOutOfBoundsError varName toR'
              else
                 return $ ()


listReplaceElemAtIndex :: [ValueT] -> Integer -> ValueT -> [ValueT]
listReplaceElemAtIndex list index value =
  let index' = fromIntegral $ index
  in take index' list ++ [value] ++ drop ((+) index' 1) list


listGetSublist :: [ValueT] -> Integer -> Integer -> [ValueT]
listGetSublist list fromR toR =
  let fromR' = fromIntegral $ fromR
      toR'   = fromIntegral $ toR
  in drop fromR' . take ((+) toR' 1) $ list



-- ****************************** --
-- *                            * --
-- * Structure Operations Tools * --
-- *                            * --
-- ****************************** --
evalVariableValue :: MainEnv -> VarName -> (ValueT, Scope)
evalVariableValue env varName =
  let varScope = getVariableScope env varName           
      mappingF = getVariableScopeFunction varScope
  in (getVariableValue env varName mappingF, varScope)
  

updateEnvWithStructureOpResult :: MainEnv -> VarName -> ValueT -> Scope -> MainEnv
updateEnvWithStructureOpResult env varName value scope =
  case scope of
    Global -> updateGlobalVariablesEnv env varName value
    Local  -> updateLocalVariablesMap env varName value


getVariableScope :: MainEnv -> VarName -> Scope
getVariableScope (MEnv _ _ varEnv _ scope) varName =
  if (||) (Set.member varName (globalVariablesInUseVEnv varEnv)) ((==) scope Global) then
     Global
     else
        Local

-- Note: It is guaranteed that variable exists.
--       Check has been done at Static Analysis stage.
getVariableValue :: MainEnv -> VarName -> (VarEnv -> VarMap) -> ValueT
getVariableValue (MEnv _ _ varEnv _ _) varName f = fromJust $ Map.lookup varName (f varEnv)


getVariableScopeFunction :: Scope -> (VarEnv -> VarMap)
getVariableScopeFunction scope =
  case scope of
    Global -> globalVariablesVEnv
    Local  -> localVariablesVEnv



-- *********************** --
-- *                     * --
-- * Update Variable Map * --
-- *                     * --
-- *********************** --
updateLocalVariablesMap :: MainEnv -> VarName -> ValueT -> MainEnv
updateLocalVariablesMap env varName value =
  let (MEnv currFunName funEnv varEnv retEnv scope) = env
      lVariables = Map.insert varName value (localVariablesVEnv varEnv)
      varEnv'    = VEnv (globalVariablesVEnv varEnv) (globalVariablesInUseVEnv varEnv) lVariables
  in MEnv currFunName funEnv varEnv' retEnv scope


updateGlobalVariablesEnv :: MainEnv -> VarName -> ValueT -> MainEnv
updateGlobalVariablesEnv env varName value =
  let (MEnv currFunName funEnv varEnv retEnv scope) = env
      gVariablesInUseVEnv'  = globalVariablesInUseVEnv varEnv
      gVariablesInUseVEnv'' = Set.insert varName gVariablesInUseVEnv'
      gVariablesInUse       = if ((/=) scope Global) then
                                 gVariablesInUseVEnv''
                                 else
                                    gVariablesInUseVEnv'
      gVariables            = Map.insert varName value (globalVariablesVEnv varEnv)
      varEnv'               = VEnv gVariables gVariablesInUse (localVariablesVEnv varEnv)
  in MEnv currFunName funEnv varEnv' retEnv scope



-- ****************** --
-- *                * --
-- * Func Expr Stmt * --
-- *                * --
-- ****************** --
restoreVarScope :: MainEnv -> VarEnv -> FunName -> (VarEnv, FunExecute)
restoreVarScope env varEnv' funName =
  let (FEDesc fExecute funParams varScope) = fromJust $ Map.lookup funName (funDesc (funEnv env))
      gVariablesInUse              = (globalVariablesVScope varScope)
      gVariables                   = globalVariablesVEnv varEnv'
      lVariablesInUse              = Set.union (localVariablesVScope varScope) (Set.fromList (orderList funParams))
      lVariables                   = localVariablesVEnv varEnv'
  in (VEnv gVariables gVariablesInUse lVariables, fExecute)


evalFuncExpr :: MainEnv -> FuncExpr -> ExpectedReturnMode -> EvalProg (ValueT, MainEnv)
evalFuncExpr env funcExpr expectedReturnMode =
  do (MEnv _ funEnv' varEnv' retEnv' _, funName) <- evalEnvWithFuncArgs env funcExpr
     let env'                  = MEnv (Just funName) funEnv' varEnv' retEnv' Local
         (varEnv'', fExecute)  = restoreVarScope env' varEnv' funName
         env''                 = MEnv (Just funName) funEnv' varEnv'' retEnv' Local
     env'''           <- evalBodyList env'' (bodyCode fExecute)
     checkExpectedReturnExprMode env''' expectedReturnMode
     let (evExpr, env'''') = getReturnExprValue env'''
         env'''''          = prepareResponseEnv env env''''
         env''''''         = unionEnvironments env env'''''
     return $ (evExpr, env'''''') 
     
     
-- Note: Thanks to Static Analysis stage there is no need of checking whether
--       all required parameters have been delivered. It is also guaranteed
--       that function call refers to a valid function name.
evalEnvWithFuncArgs :: MainEnv -> FuncExpr -> EvalProg (MainEnv, FunName)
evalEnvWithFuncArgs env funcExpr =
  case funcExpr of
    (FuncExprKeywordArgs (FuncStmtDeclIdent (Ident funName)) keywordArgs)       ->
       do env'  <- evalEnvWithDefaultFuncArgs env funName
          env'' <- evalEnvWithKeywordFuncArgs env' keywordArgs
          return $ (env'', funName)
    (FuncExprPositionalArgs (FuncStmtDeclIdent (Ident funName)) positionalArgs) ->
       do env'  <- evalEnvWithDefaultFuncArgs env funName
          let funEnvDesc = fromJust $ Map.lookup funName (funDesc (funEnv env'))
              orderList' = orderList (funParams funEnvDesc)
          env'' <- evalEnvWithPositionalFuncArgs env' orderList' positionalArgs
          return $ (env'', funName)
              

----------------------------
-- Eval Keyword Arguments --
----------------------------
evalEnvWithDefaultFuncArgs :: MainEnv -> FunName -> EvalProg MainEnv
evalEnvWithDefaultFuncArgs env funName =
  do let funEnvDesc                                    = fromJust $ Map.lookup funName (funDesc funEnv)
         defArgMapper'                                 = defArgMapper (funExecute funEnvDesc)
         (MEnv currFunName funEnv varEnv retEnv scope) = env
     evalEnvWithDefaultFuncArgs' env (Map.toList defArgMapper')


type DefArgList = [(VarName, Expr)]
evalEnvWithDefaultFuncArgs' :: MainEnv -> DefArgList -> EvalProg MainEnv
evalEnvWithDefaultFuncArgs' env []       = return $ env
evalEnvWithDefaultFuncArgs' env ((varName, expr) : xs) =
  do (evExpr, env') <- evalExpr env expr
     let env'' = updateLocalVariablesMap env varName evExpr
     evalEnvWithDefaultFuncArgs' env'' xs


evalEnvWithKeywordFuncArgs :: MainEnv ->  [KeywordArg] -> EvalProg MainEnv
evalEnvWithKeywordFuncArgs env []                                       = return $ env
evalEnvWithKeywordFuncArgs env ((KeywordArg (Ident varName) expr) : xs) =
  do (evExpr, env') <- evalExpr env expr
     let env'' = updateLocalVariablesMap env' varName evExpr
     evalEnvWithKeywordFuncArgs env'' xs


-------------------------------
-- Eval Positional Arguments --
-------------------------------
evalEnvWithPositionalFuncArgs :: MainEnv -> VarNames -> [PositionalArg] -> EvalProg MainEnv
evalEnvWithPositionalFuncArgs env _ [] = return $ env
evalEnvWithPositionalFuncArgs env [] _ = return $ env
evalEnvWithPositionalFuncArgs env (x : xs) ((PositionalArg y) : ys) =
  do (evExpr, env') <- evalExpr env y
     let env'' = updateLocalVariablesMap env' x evExpr
     evalEnvWithPositionalFuncArgs env'' xs ys


-- ************* --
-- *           * --
-- * Eval Expr * --
-- *           * --
-- ************* --
data LogicOperation = Alternative
                    | Conjunction
                    | Negation


data ComparisonOperation = NotEqual
                         | Equal
                         | GreaterEqual
                         | Greater
                         | LesserEqual
                         | Lesser


data ArithmeticOperation = Addition
                         | Subtraction
                         | Multiplication
                         | Division


--------------
-- Wrappers --
--------------
wrapAsBoolObject :: BoolT -> ValueT
wrapAsBoolObject = BoolObject


wrapAsIntegerObject :: IntegerT -> ValueT
wrapAsIntegerObject = IntegerObject


wrapAsDictObject :: DictT -> ValueT
wrapAsDictObject = DictObject


wrapAsListObject :: ListT -> ValueT
wrapAsListObject = ListObject


wrapAsTupleObject :: TupleT -> ValueT
wrapAsTupleObject = TupleObject



evalExpr :: MainEnv -> Expr -> EvalProg (ValueT, MainEnv)
evalExpr env expr =
  case expr of
    (ExprAtom atom)                   -> evalAtomExpr env atom
    
    (ExprAlternativeOp expr expr')    -> evalTwoArgLogicOp env expr expr' Alternative
    (ExprConjunctionOp expr expr')    -> evalTwoArgLogicOp env expr expr' Conjunction
    (ExprNegationOp expr)             -> evalOneArgLogicOp env expr Negation

    (ExprNotEqualOp expr expr')       -> evalTwoArgComparisonOp env expr expr' NotEqual
    (ExprEqualOp expr expr')          -> evalTwoArgComparisonOp env expr expr' Equal
    (ExprGreaterEqualOp expr expr')   -> evalTwoArgComparisonOp env expr expr' GreaterEqual
    (ExprGreaterOp expr expr')        -> evalTwoArgComparisonOp env expr expr' Greater
    (ExprLesserEqualOp expr expr')    -> evalTwoArgComparisonOp env expr expr' LesserEqual
    (ExprLesserOp expr expr')         -> evalTwoArgComparisonOp env expr expr' Lesser

    (ExprAdditionOp expr expr')       -> evalTwoArgArithmeticOp env expr expr' Addition
    (ExprSubtractionOp expr expr')    -> evalTwoArgArithmeticOp env expr expr' Subtraction
    (ExprMultiplicationOp expr expr') -> evalTwoArgArithmeticOp env expr expr' Multiplication
    (ExprDivisionOp expr expr')       -> evalTwoArgArithmeticOp env expr expr' Division
    


-- ******************* --
-- *                 * --
-- * Atom Operations * --
-- *                 * --
-- ******************* --
evalAtomExpr :: MainEnv -> Atom -> EvalProg (ValueT, MainEnv)
evalAtomExpr env atom =
  case atom of
    (AtomIdent (Ident ident))     -> evalIdentExpr env ident
    (AtomLiteral literal)         -> evalLiteralExpr env literal
    (AtomFuncExpr funcExpr)       -> evalFuncExpr env funcExpr RETURN_CASCADE
    (AtomStructure structure)     -> evalStructureExpr env structure
    (AtomStructureOp structureOp) -> evalStructureOpExpr env structureOp


--------------- ** -
-- Ident Expr - ** -
--------------- ** -
evalIdentExpr :: MainEnv -> VarName -> EvalProg (ValueT, MainEnv)
evalIdentExpr env varName =
  do let (value, scope) = evalVariableValue env varName
     return $ (value, env)


----------------- ** -
-- Literal Expr - ** -
----------------- ** -
evalLiteralExpr :: MainEnv -> Literal -> EvalProg (ValueT, MainEnv)
evalLiteralExpr env literal =
  case literal of
    (BoolLiteral bLiteral)    -> return $ (BoolObject (BT $ boolLiteralToBool bLiteral), env)
    (IntegerLiteral iLiteral) -> return $ (IntegerObject (IT iLiteral), env)


boolLiteralToBool :: Boolean -> Bool
boolLiteralToBool (Boolean bStringRepresentation) =
  case bStringRepresentation of
    "true"  -> True
    "false" -> False


------------------- ** -
-- Structure Expr - ** -
------------------- ** -
evalStructureExpr :: MainEnv -> Structure -> EvalProg (ValueT, MainEnv)
evalStructureExpr env structure =
  case structure of
    (MutableStructure mutable)     ->
       case mutable of
         (MutableStructureList list)     -> evalListStructureExpr env list []
         (MutableStructureDict dict)     -> evalDictStructureExpr env dict emptyDictObject
    (ImmutableStructure immutable) ->
       case immutable of
         (ImmutableStructureTuple tuple) -> evalTupleStructureExpr env tuple []


----------
-- List --
----------
evalListStructureExpr :: MainEnv -> List -> [ValueT] -> EvalProg (ValueT, MainEnv)
evalListStructureExpr env (List []) evList                  = return $ (ListObject (LIT evList), env)
evalListStructureExpr env (List ((ListExpr x) : xs)) evList =
  do (evExpr, env') <- evalExpr env x
     evalListStructureExpr env' (List xs) ((++) evList [evExpr])


----------
-- Dict --
----------
evalDictStructureExpr :: MainEnv -> Dict -> ValueT -> EvalProg (ValueT, MainEnv)
evalDictStructureExpr env (Dict []) evDict       = return $ (evDict, env)
evalDictStructureExpr env (Dict (x : xs)) evDict =
  case x of
    (DictKeyDatumIdent (Ident ident) expr) -> do (key, env')  <- evalIdentExpr env ident
                                                 (val, env'') <- evalExpr env expr
                                                 evDict'      <- updateDictStructure evDict key val
                                                 evalDictStructureExpr env'' (Dict xs) evDict'
    (DictKeyDatumLiteral literal expr)     -> do (key, env')  <- evalLiteralExpr env literal
                                                 (val, env'') <- evalExpr env expr
                                                 evDict'      <- updateDictStructure evDict key val
                                                 evalDictStructureExpr env'' (Dict xs) evDict'


updateDictStructure :: ValueT -> ValueT -> ValueT -> EvalProg ValueT
updateDictStructure (DictObject dObject) key val =
  do let bKeyMap = boolKeyDict dObject
         iKeyMap = intKeyDict dObject
     updateDictStructure' bKeyMap iKeyMap key val


updateDictStructure' :: BoolMap -> IntegerMap -> ValueT -> ValueT -> EvalProg ValueT
updateDictStructure' bDict iDict key val =
  case key of
    (BoolObject bObject)    -> do let bDict' = Map.insert bObject val bDict
                                  return $ DictObject (DT bDict' iDict)
    (IntegerObject iObject) -> do let iDict' = Map.insert iObject val iDict
                                  return $ DictObject (DT bDict iDict')
    otherwise               -> throwError $ invalidKeyTypeForDictError key


emptyDictObject :: ValueT
emptyDictObject = DictObject (DT Map.empty Map.empty)


-----------
-- Tuple --
-----------
evalTupleStructureExpr :: MainEnv -> Tuple -> [ValueT] -> EvalProg (ValueT, MainEnv)
evalTupleStructureExpr env (Tuple []) evTuple                   = return $ (TupleObject (TT evTuple), env)
evalTupleStructureExpr env (Tuple ((TupleExpr x) : xs)) evTuple =
  do (evExpr, env') <- evalExpr env x
     evalTupleStructureExpr env' (Tuple xs) ((++) evTuple [evExpr])


---------------------- ** -
-- Structure Op Expr - ** -
---------------------- ** -
evalListFrontElemIndex :: MainEnv -> VarName -> EvalProg (ValueT, MainEnv)
evalListFrontElemIndex env ident = return $ (IntegerObject $ IT 0, env)


evalListLastElemIndex :: MainEnv -> VarName -> EvalProg (ValueT, MainEnv)
evalListLastElemIndex env ident =
  do (IntegerObject (IT len), _) <- handleListGetLength env ident
     return $ (IntegerObject (IT $ (-) len 1), env)


evalStructureOpExpr :: MainEnv -> StructureOp -> EvalProg (ValueT, MainEnv)
evalStructureOpExpr env structureOp =
  case structureOp of
    ----------
    -- List --
    ----------
    (ListGetElemByPos (Ident ident) expr)     -> do (evExpr, env')    <- evalExpr env expr
                                                    handleListGetElemByPos env' ident evExpr 
    (ListGetSublist (Ident ident) expr expr') -> do (evExpr', env')   <- evalExpr env expr
                                                    (evExpr'', env'') <- evalExpr env' expr'
                                                    handleListGetSublist env'' ident evExpr' evExpr''
    (ListGetLhsSublist (Ident ident) expr)    -> do (evExpr, env')    <- evalExpr env expr
                                                    (index, _)        <- evalListFrontElemIndex env' ident
                                                    handleListGetSublist env' ident index evExpr
    (ListGetRhsSublist (Ident ident) expr)    -> do (evExpr, env')    <- evalExpr env expr
                                                    (index, env'')    <- evalListLastElemIndex env' ident
                                                    handleListGetSublist env'' ident evExpr index
    (ListGetFrontElem (Ident ident))          -> do (index, _)        <- evalListFrontElemIndex env ident
                                                    handleListGetElemByPos env ident index
    (ListGetLastElem (Ident ident))           -> do (index, env')     <- evalListLastElemIndex env ident
                                                    handleListGetElemByPos env' ident index
    (ListGetLength (Ident ident))             -> handleListGetLength env ident
    (ListPopFrontElem (Ident ident))          -> handleListPopFrontElem env ident
    (ListPopLastElem (Ident ident))           -> handleListPopLastElem env ident

    ----------
    -- Dict --
    ----------
    (DictGetElemByKey (Ident ident) expr)     -> do (evExpr, env')    <- evalExpr env expr
                                                    handleDictGetElemByKey env' ident evExpr
    (DictGetKey (Ident ident))                -> handleDictGetKey env ident
    (DictGetValue (Ident ident))              -> handleDictGetValue env ident
    (DictPopElemByKey (Ident ident) expr)     -> do (evExpr, env')    <- evalExpr env expr
                                                    handleDictPopElemByKey env' ident evExpr

    -----------
    -- Tuple --
    -----------
    (TupleGetElemAtPos (Ident ident) expr)    -> do (evExpr, env')    <- evalExpr env expr
                                                    handleTupleGetElemAtPos env' ident evExpr
    (TupleGetSize (Ident ident))              -> handleTupleGetSize env ident



-- ********************************* --
-- *                               * --
-- * Eval Structure Operation Expr * --
-- *                               * --
-- ********************************* --
----------
-- List --
----------
handleListGetElemByPos :: MainEnv -> VarName -> ValueT -> EvalProg (ValueT, MainEnv)
handleListGetElemByPos env varName index =
  case index of
     (IntegerObject (IT iVal)) ->
        do ((ListObject (LIT elemList)), env') <- handleListGetSublist env varName index index
           return $ (head $ elemList, env')
     otherwise                 -> throwError $ notIntegerIndexInCollectionError index varName
     

handleListGetSublist :: MainEnv -> VarName -> ValueT -> ValueT -> EvalProg (ValueT, MainEnv)
handleListGetSublist env varName fromR toR =
  do let (evValue, varScope) = evalVariableValue env varName
     case evValue of
       (ListObject (LIT list)) ->
          do (IntegerObject (IT length), _) <- handleListGetLength env varName
             case (fromR, toR) of
               (IntegerObject (IT iVal), IntegerObject (IT iVal')) ->
                  do listRateRequestedRange varName list iVal iVal'
                     let sublistContents = listGetSublist list iVal iVal'
                     return $ (ListObject (LIT sublistContents), env)
               otherwise                                           ->
                  throwError $ sublistBoundsNotIntegersError fromR toR
       otherwise               -> throwError $ notListCollectionError varName


handleListGetLength :: MainEnv -> VarName -> EvalProg (ValueT, MainEnv)
handleListGetLength env varName =
  do let (evValue, varScope) = evalVariableValue env varName
     case evValue of
       (ListObject (LIT list)) -> return $ (IntegerObject $ IT $ fromIntegral $ length list, env)
       otherwise               -> throwError $ lengthOnNonListCollectionError varName


handleListPopFrontElem :: MainEnv -> VarName -> EvalProg (ValueT, MainEnv)
handleListPopFrontElem env varName =
  do let (evValue, varScope) = evalVariableValue env varName
     case evValue of
       (ListObject (LIT list)) -> do (IntegerObject (IT last), _)  <- handleListGetLength env varName
                                     let lastIndex' = IntegerObject $ IT $ last - 1
                                         nextIndex  = IntegerObject $ IT 1
                                     (frontIndex, _) <- evalListFrontElemIndex env varName
                                     (retVal, _)     <- handleListGetElemByPos env varName frontIndex
                                     (lastIndex, _)  <- handleListGetLength env varName
                                     (newList, _)    <- case (nextIndex, lastIndex') of
                                                          (IntegerObject (IT 1), IntegerObject (IT (0))) ->
                                                             return $ (ListObject $ LIT [], env)
                                                          otherwise                                     ->
                                                             handleListGetSublist env varName nextIndex lastIndex'
                                     return $ (retVal, updateEnvWithStructureOpResult env varName newList varScope)
       otherwise               -> throwError $ popFrontOnNonListCollectionError varName


handleListPopLastElem :: MainEnv -> VarName -> EvalProg (ValueT, MainEnv)
handleListPopLastElem env varName =
  do let (evValue, varScope) = evalVariableValue env varName
     case evValue of
       (ListObject (LIT list)) -> do (IntegerObject (IT last), _)  <- handleListGetLength env varName
                                     let previousIndex = IntegerObject $ IT $ last - 2
                                         lastIndex     = IntegerObject $ IT $ last - 1
                                     (frontIndex, _) <- evalListFrontElemIndex env varName
                                     (retVal, _)     <- handleListGetElemByPos env varName lastIndex
                                     (newList, _)    <- case (frontIndex, previousIndex) of
                                                          (IntegerObject (IT 0), IntegerObject (IT (-1))) ->
                                                             return $ (ListObject $ LIT [], env)
                                                          otherwise                                     ->
                                                             handleListGetSublist env varName frontIndex previousIndex
                                     return $ (retVal, updateEnvWithStructureOpResult env varName newList varScope)
       otherwise               -> throwError $ popLastOnNonListCollectionError varName


----------
-- Dict --
----------
handleDictGetElemByKey :: MainEnv -> VarName -> ValueT -> EvalProg (ValueT, MainEnv)
handleDictGetElemByKey env varName key =
  let (evValue, varScope) = evalVariableValue env varName
  in case evValue of
       (DictObject dObject) -> do value <- dictRateRequestedKeyExists dObject key
                                  return $ (value, env)
       otherwise            -> throwError $ invalidDictObjectError varName

          
boolListElemWrapper :: (BoolT, ValueT) -> (ValueT, ValueT)
boolListElemWrapper (bVal, value) = (wrapAsBoolObject $ bVal, value)


integerListElemWrapper :: (IntegerT, ValueT) -> (ValueT, ValueT)
integerListElemWrapper (iVal, value) = (wrapAsIntegerObject $ iVal, value)


handleDictIsSingletonInstance :: MainEnv -> VarName -> EvalProg ((ValueT, ValueT), MainEnv)
handleDictIsSingletonInstance env varName =
  do let (evValue, varScope) = evalVariableValue env varName
     case evValue of
       (DictObject dObject) -> do let bkList = Map.toList (boolKeyDict dObject)
                                      ikList = Map.toList (intKeyDict dObject)
                                  if (+) (length bkList) (length ikList) == 1 then
                                     if (==) (length bkList) 1 then
                                        return $ (boolListElemWrapper $ head $ bkList, env)
                                        else
                                           return $ (integerListElemWrapper $ head $ ikList, env)
                                     else
                                        throwError $ notSingletonDictInstanceError varName
       otherwise            -> throwError $ invalidDictObjectError varName


handleDictGetKey :: MainEnv -> VarName -> EvalProg (ValueT, MainEnv)
handleDictGetKey env varName =
  do ((key, value), _) <- handleDictIsSingletonInstance env varName
     return $ (key, env)


handleDictGetValue :: MainEnv -> VarName -> EvalProg (ValueT, MainEnv)
handleDictGetValue env varName =
  do ((key, value), _) <- handleDictIsSingletonInstance env varName
     return $ (value, env)
       

handleDictPopElemByKey :: MainEnv -> VarName -> ValueT -> EvalProg (ValueT, MainEnv)
handleDictPopElemByKey env varName key =
  let (evValue, varScope) = evalVariableValue env varName
  in case evValue of
       (DictObject dObject) ->
          do value <- dictRateRequestedKeyExists dObject key
             let modifiedDict = dictDeleteKey dObject key
             return $ (value, updateEnvWithStructureOpResult env varName modifiedDict varScope)
       otherwise            -> throwError $ invalidDictObjectError varName


-----------
-- Tuple --
-----------
handleTupleGetElemAtPos :: MainEnv -> VarName -> ValueT -> EvalProg (ValueT, MainEnv)
handleTupleGetElemAtPos env varName index =
  case index of
    (IntegerObject (IT iVal)) ->
       do let (evValue, varScope) = evalVariableValue env varName
          case evValue of
            (TupleObject (TT list)) -> do listRateRequestedRange varName list iVal iVal
                                          return $ (head $ listGetSublist list iVal iVal, env)
            otherwise               -> throwError $ invalidTupleObjectError varName
    otherwise                 -> throwError $ notIntegerIndexInCollectionError index varName


handleTupleGetSize :: MainEnv -> VarName -> EvalProg (ValueT, MainEnv)
handleTupleGetSize env varName =
  do let (evValue, varScope) = evalVariableValue env varName
     case evValue of
       (TupleObject (TT list)) -> return $ (IntegerObject $ IT $ fromIntegral $ length list, env)
       otherwise               -> throwError $ sizeOnNonTupleCollectionError varName


-- ******************** --
-- *                  * --
-- * Logic Operations * --
-- *                  * --
-- ******************** --
evalTwoArgLogicOp :: MainEnv -> Expr -> Expr -> LogicOperation -> EvalProg (ValueT, MainEnv)
evalTwoArgLogicOp env expr expr' logicOp =
  do (evExpr, env')   <- evalExpr env expr
     (evExpr', env'') <- evalExpr env' expr'
     case logicOp of
       Alternative ->
         return $ (wrapAsBoolObject $ DataTypes.and (toBoolT evExpr) (toBoolT evExpr'), env'')
       Conjunction ->
         return $ (wrapAsBoolObject $ DataTypes.and (toBoolT evExpr) (toBoolT evExpr'), env'')
    

evalOneArgLogicOp :: MainEnv -> Expr -> LogicOperation -> EvalProg (ValueT, MainEnv)
evalOneArgLogicOp env expr logicOp =
  do (evExpr, env') <- evalExpr env expr
     case logicOp of
       Negation -> return $ (wrapAsBoolObject $ DataTypes.not $ toBoolT evExpr, env)



-- ************************* --
-- *                       * --
-- * Comparison Operations * --
-- *                       * --
-- ************************* --
evalTwoArgComparisonOp :: MainEnv -> Expr -> Expr -> ComparisonOperation -> EvalProg (ValueT, MainEnv)
evalTwoArgComparisonOp env expr expr' compOp =
  do (evExpr, env')   <- evalExpr env expr
     (evExpr', env'') <- evalExpr env' expr'
     case compOp of
       Equal        -> do comparisonR <- evalEqualOp evExpr evExpr'
                          return $ (comparisonR, env'')
       NotEqual     -> do comparisonR <- evalNotEqualOp evExpr evExpr'
                          return $ (comparisonR, env'')
       GreaterEqual -> do comparisonR <- evalGreaterEqualOp evExpr evExpr'
                          return $ (comparisonR, env'')
       Greater      -> do comparisonR <- evalGreaterOp evExpr evExpr'
                          return $ (comparisonR, env'')
       LesserEqual  -> do comparisonR <- evalLesserEqualOp evExpr evExpr'
                          return $ (comparisonR, env'')
       Lesser       -> do comparisonR <- evalLesserOp evExpr evExpr'
                          return $ (comparisonR, env'')


-----------------
-- Equal Op --
-----------------
equalCerifificate :: (Eq a) => a -> a -> ValueT
equalCerifificate obj obj' = wrapAsBoolObject $ BT $ (==) obj obj'


evalEqualOp :: ValueT -> ValueT -> EvalProg ValueT
evalEqualOp lhsV rhsV =
  case (lhsV, rhsV) of
    (BoolObject bObject, BoolObject bObject')       -> return $ equalCerifificate bObject bObject'
    (IntegerObject iObject, IntegerObject iObject') -> return $ equalCerifificate iObject iObject'
    (DictObject dObject, DictObject dObject')       -> return $ equalCerifificate dObject dObject
    (ListObject lObject, ListObject lObject')       -> return $ equalCerifificate lObject lObject'
    (TupleObject tObject, TupleObject tObject')     -> return $ equalCerifificate tObject tObject
    otherwise                                       -> throwError $ invalidComparisonOperandsError lhsV rhsV


--------------
-- NotEqual Op --
--------------
evalNotEqualOp :: ValueT -> ValueT -> EvalProg ValueT
evalNotEqualOp lhsV rhsV =
  do (BoolObject bObject) <- evalEqualOp lhsV rhsV
     return $ BoolObject $ DataTypes.not bObject


----------------------
-- Greater Equal Op --
----------------------
greaterEqualCertificate :: IntegerT -> IntegerT -> ValueT
greaterEqualCertificate obj obj' = wrapAsBoolObject $ greaterEqual obj obj'


evalGreaterEqualOp :: ValueT -> ValueT -> EvalProg ValueT
evalGreaterEqualOp lhsV rhsV =
  case (lhsV, rhsV) of
    (IntegerObject iObject, IntegerObject iObject') -> return $ greaterEqualCertificate iObject iObject'
    otherwise                                       -> throwError $ invalidComparisonOperandsError lhsV rhsV


----------------
-- Greater Op --
----------------
evalGreaterOp :: ValueT -> ValueT -> EvalProg ValueT
evalGreaterOp lhsV rhsV =
  do (BoolObject bObject) <- evalGreaterEqualOp lhsV rhsV
     return $ BoolObject $ DataTypes.not bObject


---------------------
-- Lesser Equal Op --
---------------------
lesserEqualCertificate :: IntegerT -> IntegerT -> ValueT
lesserEqualCertificate obj obj' = wrapAsBoolObject $ lesserEqual obj obj'


evalLesserEqualOp :: ValueT -> ValueT -> EvalProg ValueT
evalLesserEqualOp lhsV rhsV =
  case (lhsV, rhsV) of
    (IntegerObject iObject, IntegerObject iObject') -> return $ lesserEqualCertificate iObject iObject'
    otherwise                                       -> throwError $ invalidComparisonOperandsError lhsV rhsV


---------------
-- Lesser Op --
---------------
evalLesserOp :: ValueT -> ValueT -> EvalProg ValueT
evalLesserOp lhsV rhsV =
  do (BoolObject bObject) <- evalLesserEqualOp lhsV rhsV
     return $ BoolObject $ DataTypes.not bObject



-- ************************** --
-- *                        * --
-- * Arithmetics Operations * --
-- *                        * --
-- ************************** --
evalTwoArgArithmeticOp :: MainEnv -> Expr -> Expr -> ArithmeticOperation -> EvalProg (ValueT, MainEnv)
evalTwoArgArithmeticOp env expr expr' arithmOp =
  do (evExpr, env')   <- evalExpr env expr
     (evExpr', env'') <- evalExpr env' expr'
     case arithmOp of
       Addition       -> do arithmR <- evalAdditionOp evExpr evExpr'
                            return $ (arithmR, env'')
       Subtraction    -> do arithmR <- evalSubtractionOp evExpr evExpr'
                            return $ (arithmR, env'')
       Multiplication -> do arithmR <- evalMultiplicationOp evExpr evExpr'
                            return $ (arithmR, env'')
       Division       -> do arithmR <- evalDivisionOp evExpr evExpr'
                            return $ (arithmR, env'')

evalAdditionOp :: ValueT -> ValueT -> EvalProg ValueT
evalAdditionOp lhsV rhsV =
  case (lhsV, rhsV) of
    (IntegerObject iObject, IntegerObject iObject') -> return $ wrapAsIntegerObject $ add iObject iObject'
    (DictObject dObject, DictObject dObject')       -> return $ wrapAsDictObject $ union dObject dObject'
    (ListObject lObject, ListObject lObject')       -> return $ wrapAsListObject $ union lObject lObject'
    otherwise                                       -> throwError $ invalidAdditionOperationOperandsError lhsV rhsV


evalSubtractionOp :: ValueT -> ValueT -> EvalProg ValueT
evalSubtractionOp lhsV rhsV =
  case (lhsV, rhsV) of
    (IntegerObject iObject, IntegerObject iObject') -> return $ wrapAsIntegerObject $ DataTypes.subtract iObject iObject'
    otherwise                                       -> throwError $ arithmeticOperationOperandsError "-" lhsV rhsV


evalMultiplicationOp :: ValueT -> ValueT -> EvalProg ValueT
evalMultiplicationOp lhsV rhsV =
  case (lhsV, rhsV) of
    (IntegerObject iObject, IntegerObject iObject') -> return $ wrapAsIntegerObject $ multiply iObject iObject'
    otherwise                                       -> throwError $ arithmeticOperationOperandsError "*" lhsV rhsV


evalDivisionOp :: ValueT -> ValueT -> EvalProg ValueT
evalDivisionOp lhsV rhsV =
  case (lhsV, rhsV) of
    (IntegerObject iObject, IntegerObject iObject') -> do let zero = (IT 0)
                                                          if (==) iObject' zero then
                                                             throwError $ divisionByZeroError
                                                             else
                                                                return $ wrapAsIntegerObject $ divide iObject iObject'
    otherwise                                       -> throwError $ arithmeticOperationOperandsError "div" lhsV rhsV

