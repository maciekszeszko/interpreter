module StaticAnalyzerEnvironment where

import qualified Data.Map as Map
import qualified Data.Set as Set


type FunName     = String
type VarName     = String

data Scope       = Global | Local deriving (Eq, Show)

data ExprType    = BoolT
                 | IntegerT
                 | MutableListT
                 | MutableDictT
                 | ImmutableTupleT
                   deriving (Eq, Ord, Show)

type ExprTypes   = [ExprType]
type VarType     = ExprType
type VarTypes    = [VarType]
type VarSet      = Set.Set VarName
type VarTypeSet  = Set.Set VarType
type VarOrd      = [VarName]
type SAFEnv      = Map.Map FunName SAFDesc
type FunState    = Maybe FunName


getAllVarTypes :: ExprTypes
getAllVarTypes = [BoolT, IntegerT, MutableListT, MutableDictT, ImmutableTupleT]


-- Static Analyzer Function Descriptor --
data SAFDesc     = SAFDesc {
                     defArgs :: VarSet,
                     args    :: VarSet,
                     argsOrd :: VarOrd,
                     proc    :: Bool,
                     returnT :: VarTypeSet
                   } deriving (Show)


-- Static Analyzer Environment --
data SAEnv       = SAEnv {
                     funEnv         :: SAFEnv,
                     currentFun     :: FunState,
                     localVarEnv    :: VarSet,
                     globalVarEnv   :: VarSet,
                     globalVarInUse :: VarSet,
                     scope          :: Scope
                   } deriving (Show)

