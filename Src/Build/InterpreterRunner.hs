module Main where

import System.IO (stdin, hGetContents)
import System.Environment (getArgs, getProgName)

import LexGrammar
import ParGrammar
import SkelGrammar
import PrintGrammar
import AbsGrammar

import Interpreter (runEvalProg, evalProg, emptyEnv)
import StaticAnalyzerMain (runEvalTree, evalTree, emptyEnv)

import Control.Monad.Error
import Control.Monad.Identity
import Control.Monad.IO.Class

import ErrM


type ParseFun a = [Token] -> Err a
type Res     = ()

myLLexer = myLexer

type ParseProg a = ErrorT String IO a

run :: ParseFun Program -> String -> ParseProg Program
run p s =
  let ts = myLLexer s
  in case p ts of
       Bad s   -> throwError $ "Parser ERROR: " ++ (show s) ++ "."
       Ok tree -> return $ tree


processProgram :: String -> ParseProg Res
processProgram pText = do program <- run pProgram pText
                          evalProg Interpreter.emptyEnv program


processTree :: String -> ParseProg Res
processTree pText = do tree <- run pProgram pText
                       evalTree StaticAnalyzerMain.emptyEnv tree


prepareReport :: Either String a -> IO ()
prepareReport (Left err) = putStrLn $ err
prepareReport (Right _)  = putStrLn $ "-----------------------------------\n" ++
                                      "Successfully passed script interpreting!"


main :: IO ()      
main = do args <- getArgs
          case args of
            [f]       -> do text <- readFile f
                            tRes <- runEvalProg (processTree text)
                            case tRes of
                              Left errTree -> prepareReport tRes
                              Right _      -> do pRes <- runEvalProg (processProgram text)
                                                 prepareReport pRes
            otherwise -> printUsage


printUsage :: IO ()
printUsage = do pName <- getProgName
                putStrLn $ "Usage: " ++ pName ++ " [file]"

