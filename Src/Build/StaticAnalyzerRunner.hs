module Main where

import System.IO (stdin, hGetContents)
import System.Environment (getArgs, getProgName)

import LexGrammar
import ParGrammar
import SkelGrammar
import PrintGrammar
import AbsGrammar

import StaticAnalyzerMain

import Control.Monad.Error
import Control.Monad.Identity
import Control.Monad.IO.Class

import ErrM


type ParseFun a = [Token] -> Err a
type StaticAnalysisResult = StaticAnalyzerMain.Result

myLLexer = myLexer

type ParseProgTree a = ErrorT String IO a

run :: ParseFun Program -> String -> ParseProgTree Program
run p s =
  let ts = myLLexer s
  in case p ts of
       Bad s   -> throwError $ "Parser ERROR: " ++ (show s) ++ "."
       Ok tree -> return $ tree


processProgramTree :: String -> ParseProgTree StaticAnalysisResult
processProgramTree pText = do programTree <- run pProgram pText
                              evalProg emptyEnv programTree


prepareReport :: Either String a -> IO ()
prepareReport (Left err) = putStrLn $ err
prepareReport (Right _)  = putStrLn $ "Successfully passed static analysis stage!"


main :: IO ()      
main = do args <- getArgs
          case args of
            [f]       -> do text <- readFile f
                            pRes  <- runEvalTree (processProgramTree text)
                            prepareReport pRes
            otherwise -> printUsage


printUsage :: IO ()
printUsage = do pName <- getProgName
                putStrLn $ "Usage: " ++ pName ++ " [file]"

