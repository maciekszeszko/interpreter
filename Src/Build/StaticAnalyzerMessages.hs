module StaticAnalyzerMessages where

import AbsGrammar

errorHeader   = "Static analyzer ERROR: "
warningHeader = "Static analyzer WARNING: "


-- ********** --
--            --
--   Errors   --
--            --
-- ********** --
assignmentSyntaxError                    = errorHeader ++ "#[variables] /= #[expressions] in assignment statement."
returnStmtInGlobalScopeError             = errorHeader ++ "invalid use of return statement [Forbidden in global scope]."
redundantGlobalScopeKeywordError         = errorHeader ++ "redundant use of `global` keyword. Already in global scope."
ambiguousArgumentsError arg funName      = errorHeader ++ "ambiguous arguments of name `" ++ arg ++ "` in function `" ++ funName ++ "`."
nonIterableObjectInForeachStmtError      = errorHeader ++ "iteration over non-iterable object is strictly forbidden."
invalidNumberOfPositionalArgsError
  funName expectedParams usedParams      = errorHeader ++ "invalid number of positional arguments in function `" ++ funName ++
                                         "`.\nExpected: " ++ (show expectedParams) ++ ", used: " ++ (show usedParams) ++ "."
undefinedFunctionError funName           = errorHeader ++ "function `" ++ funName ++ "` has not been defined yet."
undefinedVarInFuncExpr varName funName   = errorHeader ++ "undefined variable `" ++ varName ++ "` in function call `" ++ funName ++ "`."
undefinedVarInExprError varName          = errorHeader ++ "undefined variable `" ++ varName ++ "`."
unspecifiedKeywordArgsError args funName = errorHeader ++ "keyword arguments `" ++ (show args) ++ "` have not been specified for functon `" ++ funName ++ "`."
invalidKeywordArgError arg funName       = errorHeader ++ "no `" ++ arg ++ "` keyword in function `" ++ funName ++ "`."
invalidVariableTypeFunError
  varName funName expectedType varType   = errorHeader ++ "invalid variable type for `" ++ varName ++ "` in function `" ++
                                           funName ++ "`.\nExpected types: " ++ (show expectedType) ++ ", Actual types: " ++ (show varType) ++ "."
divergentExprTypesError exprType
  exprType'                              = errorHeader ++ "divergent expression types for `" ++ (show exprType) ++ "` and `" ++
                                           (show exprType') ++ "`."
invalidGlobalIdentReferenceError ident   = errorHeader ++ "invalid reference to a global identifier `" ++ ident ++ "`."
procedureReturnError funName             = errorHeader ++ "procedure `" ++ funName ++ "` does not return value.\n" ++
                                           "In function expression `" ++ funName ++ "()`."
unsuitableValuationForVarError varName   = errorHeader ++ "there exists no suitable valuation for `" ++ varName ++
                                           "` that meets all inclusive expressions."
unsuitableValuationForArgError arg 
  funName                                = errorHeader ++ "there exists no suitable valuation for argument `" ++
                                           arg ++ "` of function `" ++ funName ++ "`."
unsuitableExprTypeForStructureOpError
  ident operation expectedExprTypes
  exprTypes                              = errorHeader ++ "unsuitable expression type in operation `" ++ operation ++
                                           "` for ident `" ++ ident ++ "`.\nExpected types: " ++ (show expectedExprTypes) ++
                                           ", Actual types: " ++ (show exprTypes) ++ "."
                                           



-- ************ --
--              --
--   Warnings   --
--              --
-- ************ --
redeclarationOfGlobalVariableWarning varName = warningHeader ++ "variable `" ++ varName ++ "` has just been referenced as `global`."
