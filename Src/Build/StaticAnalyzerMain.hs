module StaticAnalyzerMain where

import AbsGrammar

import Control.Monad.Error
import Control.Monad.IO.Class

import Data.Maybe

import qualified Data.List as List
import qualified Data.Map as Map
import qualified Data.Set as Set

import StaticAnalyzerEnvironment
import StaticAnalyzerMessages


emptyEnv :: SAEnv
emptyEnv = let emptySAFEnv = Map.fromList []
               emptyVarSet = Set.fromList []
           in SAEnv emptySAFEnv Nothing emptyVarSet emptyVarSet emptyVarSet Global


type Result     = ()
type EvalTree a = ErrorT String IO a

runEvalTree :: EvalTree a -> IO (Either String a)
runEvalTree tree = runErrorT tree



-- *********** --
-- *         * --
-- * Program * --
-- *         * --
-- *********** --
evalTree :: SAEnv -> Program -> EvalTree Result
evalTree env (Prog [])       = return $ ()
evalTree env (Prog (x : xs)) = 
  case x of
    ProgStmt stmt         -> do env' <- evalStmt env stmt
                                evalTree env' (Prog xs)
    ProgFuncExpr funcExpr -> do evalFuncExpr env funcExpr  -- Function expression is supposed to 
                                evalTree env (Prog xs)     -- neither modify the environment nor being used as
                                                           -- intermediate state in static analysis.



-- ******** --
-- *      * --  Note: Any statement with evaluated expression has to return the environment.
-- * Stmt * --
-- *      * --
-- ******** --
evalStmt :: SAEnv -> Stmt -> EvalTree SAEnv
evalStmt env stmt = 
  case stmt of
    PrintStmt printList                            -> evalPrintStmt env printList
    AssignStmt identList expr                      -> evalAssignStmt env identList expr
    FuncStmt declStmt bodyList                     -> evalFuncStmt env declStmt bodyList
    CondStmtBasic ifStmt elifStmtList              -> evalBasicCondStmt env ifStmt elifStmtList
    CondStmtComplexed ifStmt elifStmtList elseStmt -> evalComplexedCondStmt env ifStmt elifStmtList elseStmt
    ForeachLoopStmt iter collection bodyList       -> evalForeachLoopStmt env iter collection bodyList
    WhileLoopStmt expr bodyList                    -> evalWhileLoopStmt env expr bodyList
    StructureOpStmt structureOpStmt                -> evalStructureOpStmt env structureOpStmt




-- ************** --
-- *            * --
-- * Print Stmt * --
-- *            * --
-- ************** --
evalPrintStmt :: SAEnv -> [PrintExpr] -> EvalTree SAEnv
evalPrintStmt env []                   = return $ env      
evalPrintStmt env ((PrintExpr x) : xs) =
  do evalExprTypes env x  -- Expression types do not matter there,
     evalPrintStmt env xs -- because they do not effect environment state.



-- *************** --
-- *             * --
-- * Assign Stmt * --
-- *             * --
-- *************** --
evalAssignStmt :: SAEnv -> [AssignIdent] -> [AssignExpr] -> EvalTree SAEnv
evalAssignStmt env identList exprList =
  if (/=) (length identList) (length exprList) then
     throwError $ assignmentSyntaxError
     else
        evalAssignStmt' env identList exprList


evalAssignStmt' :: SAEnv -> [AssignIdent] -> [AssignExpr] -> EvalTree SAEnv
evalAssignStmt' env [] [] = return $ env
evalAssignStmt' env (x : xs) ((AssignExpr y) : ys) =
  case x of
    (AssignIdent (Ident ident))              -> do evalExprTypes env y
                                                   evalAssignStmt' (addVariableToEnv env ident) xs ys
    (AssignListElemIdent (Ident ident) expr) -> do evalReadNWriteIdentVarTypes env ident
                                                   evExprTypes <- evalExprTypes env expr
                                                   verifyIntegerStructureIndexType evExprTypes ident "List: [E]"
                                                   evalExprTypes env y
                                                   evalAssignStmt' (addVariableToEnv env ident) xs ys


addVariableToEnv :: SAEnv -> VarName -> SAEnv
addVariableToEnv (SAEnv fEnv fState lVarEnv gVarEnv gVarInUse scope) varName =
  -- Propagate infos to adequate scope variables
  if (||) (Set.member varName gVarInUse) ((==) scope Global) then
     -- modified global environment
     (SAEnv fEnv fState lVarEnv (Set.insert varName gVarEnv) gVarInUse Global)
     else
        -- modified local environment
        (SAEnv fEnv fState (Set.insert varName lVarEnv) gVarEnv gVarInUse Local)



-- ************* --
-- *           * --
-- * Func Stmt * --
-- *           * --
-- ************* --
evalFuncStmt :: SAEnv -> FuncStmtDecl -> [GenericStmtBody] -> EvalTree SAEnv
evalFuncStmt env declStmt bodyList =
  do env' <- evalDeclStmt env declStmt
     let env'' = env'
     env''' <- evalBodyList env'' bodyList
     updateFunctionEnvOnClose env env'''


-- Mandatory step on getting out of the function scope.
--
-- The only thing to be done there is to extract object specific data,
-- which brings info of whether it is function or procedure.
-- In former case we'll get also possible return types list.
-- Intentionally, we do not update variables in program environment,
-- because this is strictly correlated with order of function calls,
-- which can lead to unpredictable changes in program variables types.
updateFunctionEnvOnClose :: SAEnv -> SAEnv -> EvalTree SAEnv
updateFunctionEnvOnClose (SAEnv fEnv fState lVarEnv gVarEnv gVarInUse sc) env' =
  do let funName = fromJust $ currentFun $ env'
         safDesc = fromJust $ Map.lookup funName $ funEnv  env'
         funEnv' = Map.insert funName safDesc fEnv
     return $ SAEnv funEnv' fState lVarEnv gVarEnv gVarInUse sc


------------------------------
-- Generic Stmt Body Return --
------------------------------
noteHasReturnStmt :: SAEnv -> VarTypes -> EvalTree SAEnv
noteHasReturnStmt (SAEnv fEnv fState lVarEnv gVarEnv gVarInUse scope) varTypes =
  case scope of
    Global -> throwError $ returnStmtInGlobalScopeError
    Local  -> do let funName                                     = fromJust fState
                 
                     (SAFDesc defArgs args argsOrd proc returnT) = fromJust $ Map.lookup funName fEnv
                     returnT'                                    = Set.union  (Set.fromList varTypes)  returnT
                     funEnv'                                     = Map.insert funName (SAFDesc defArgs args argsOrd False returnT') fEnv
                 return $ SAEnv funEnv' fState lVarEnv gVarEnv gVarInUse scope


-----------------------
-- Generic Stmt Body --
-----------------------
-- Note: propagate return states of functions.
updateFunEnv :: SAEnv -> SAEnv -> SAEnv
updateFunEnv env env' =
  let (SAEnv fEnv currFun lVarEnv gVarEnv gVarInUse scope) = env
      (SAEnv fEnv' _ _ _ _ _)                              = env'
  in SAEnv fEnv' currFun lVarEnv gVarEnv gVarInUse scope


-- Note: Guarantee correct scopes when first defining variables
evalBodyList :: SAEnv -> [GenericStmtBody] -> EvalTree SAEnv
evalBodyList env bodyList = do env'  <- evalBodyList' env bodyList
                               return $ updateFunEnv env env'


evalBodyList' :: SAEnv -> [GenericStmtBody] -> EvalTree SAEnv
evalBodyList' env []       = return $ env
evalBodyList' env (x : xs) =
  case x of
    GenericStmtBodyStmt stmt         -> do env' <- evalStmt env stmt
                                           evalBodyList' env' xs
    GenericStmtBodyGlobalIdentStmt (GlobalIdentStmt (Ident varName)) -> 
      do evalReadOnlyIdentVarTypes env varName
         env' <- reactOnGlobalIdentStmt env varName
         evalBodyList' env' xs                -- Global variable is always read-only for free,
                                              -- even without preceding declaration, so it can
                                              -- be used as print argument or right hand side
                                              -- operand in special cases.

    GenericStmtBodyFuncExpr funcExpr -> do evalFuncExpr env funcExpr
                                           evalBodyList' env xs
    GenericStmtBodyReturn expr       -> do evExprTypes <- evalExprTypes env expr
                                           env'        <- noteHasReturnStmt env evExprTypes
                                           evalBodyList' env' xs
                                               

printWarningOnCondition :: Bool -> String -> EvalTree Result
printWarningOnCondition cond warningMsg =
  if cond then
     liftIO $ putStrLn $ warningMsg
     else
        return $ ()


reactOnGlobalIdentStmt :: SAEnv -> VarName -> EvalTree SAEnv
reactOnGlobalIdentStmt (SAEnv fEnv fState lVarEnv gVarEnv gVarInUse scope) varName =
    case scope of
      Global -> throwError $ redundantGlobalScopeKeywordError
      Local  -> do let cond    = Set.member varName gVarInUse
                       warning = redeclarationOfGlobalVariableWarning varName
                   printWarningOnCondition cond warning
                   let globalVarInUse' = Set.insert varName gVarInUse
                   return $ (SAEnv fEnv fState lVarEnv gVarEnv globalVarInUse' scope)


--------------------
-- Func Decl Stmt --
--------------------
evalDeclStmt :: SAEnv -> FuncStmtDecl -> EvalTree SAEnv
evalDeclStmt env (FuncStmtDecl (FuncStmtDeclIdent (Ident funName)) funArgs) =
  do funArgNamesOnly <- getOnlyArgNames funArgs [] env
     (defArgsSet, argsSet, argsOrd) <- evalDeclStmt' funArgs funArgNamesOnly funName
     return $ addFunctionDeclToEnv env funName defArgsSet argsSet argsOrd


evalDeclStmt' :: [FuncStmtArg] -> [VarName] -> FunName -> EvalTree (VarSet, VarSet, VarOrd)
evalDeclStmt' funArgs funArgNamesOnly funName =
  do let ambiguousArgs = filter(\x -> (>) (length x) 1) $ List.group $ List.sort $ funArgNamesOnly
     if not $ null $ ambiguousArgs then
        throwError $ ambiguousArgumentsError (head $ head ambiguousArgs) funName
        else
           evalDeclStmt'' Set.empty Set.empty [] funArgs


evalDeclStmt'' :: VarSet -> VarSet -> VarOrd -> [FuncStmtArg] -> EvalTree (VarSet, VarSet, VarOrd)
evalDeclStmt'' defArgsSet argsSet ordering []       = return $ (defArgsSet, argsSet, ordering)
evalDeclStmt'' defArgsSet argsSet ordering (x : xs) =
  case x of
    FuncStmtDeclArgStrict (Ident arg')          ->
      evalDeclStmt'' defArgsSet (Set.insert arg' argsSet) (ordering ++ [arg']) xs
    FuncStmtDeclArgDefault (Ident arg') literal ->
      evalDeclStmt'' (Set.insert arg' defArgsSet) argsSet (ordering ++ [arg']) xs


addFunctionDeclToEnv :: SAEnv -> FunName -> VarSet -> VarSet -> VarOrd -> SAEnv
addFunctionDeclToEnv (SAEnv fEnv fState lVarEnv gVarEnv gVarInUse scope) funName defArgsSet argsSet argsOrd =
  let safDesc      = SAFDesc defArgsSet argsSet argsOrd True Set.empty
      localVarEnv' = Set.union defArgsSet $ Set.union argsSet lVarEnv

  -- Since we are in function declaration we cannot make any assumptions on
  -- actual local and globar variables types. Hence it is crucial to assume
  -- that those can be any valid variable type defined for this stage.
  in SAEnv (Map.insert funName safDesc fEnv) (Just funName) localVarEnv' gVarEnv gVarInUse Local


getOnlyArgNames :: [FuncStmtArg] -> [VarName] -> SAEnv -> EvalTree [VarName]
getOnlyArgNames [] varNames _         = return $ varNames
getOnlyArgNames (x : xs) varNames env =
  case x of
    (FuncStmtDeclArgStrict (Ident varName))       -> getOnlyArgNames xs (varNames ++ [varName]) env
    (FuncStmtDeclArgDefault (Ident varName) expr) -> do evalExprTypes env expr
                                                        getOnlyArgNames xs (varNames ++ [varName]) env



-- ************************************* --
-- *                                   * --
-- * Update Non Function Stmt on Close * --
-- *                                   * --
-- ************************************* --
updateNonFunctionalStmtOnClose :: SAEnv -> SAEnv -> EvalTree SAEnv
updateNonFunctionalStmtOnClose = updateFunctionEnvOnClose


-- ******************* --
-- *                 * --
-- * Cond Stmt Basic * --
-- *                 * --
-- ******************* --
evalBasicCondStmt :: SAEnv -> CondStmtIf -> [CondStmtElif] -> EvalTree SAEnv
evalBasicCondStmt env ifStmt elifStmtList =
  do env' <- evalCondStmtIf env ifStmt
     env''  <- updateNonFunctionalStmtOnClose env env'
     env''' <- evalCondStmtElifList env'' elifStmtList
     updateNonFunctionalStmtOnClose env'' env'''



-- *********************** --
-- *                     * --
-- * Cond Stmt Complexed * --
-- *                     * --
-- *********************** --
evalComplexedCondStmt :: SAEnv -> CondStmtIf -> [CondStmtElif] -> CondStmtElse -> EvalTree SAEnv
evalComplexedCondStmt env ifStmt elifStmtList elseStmt =
  do env'     <- evalCondStmtIf env ifStmt
     env''    <- updateNonFunctionalStmtOnClose env env'
     env'''   <- evalCondStmtElifList env'' elifStmtList
     env''''  <- updateNonFunctionalStmtOnClose env'' env'''
     env''''' <- evalCondStmtElse env'''' elseStmt
     updateNonFunctionalStmtOnClose env'''' env'''''


-- *************** --
-- *             * --
-- * Cond Stmt's * --
-- *             * --
-- *************** --
evalCondStmtIf :: SAEnv -> CondStmtIf -> EvalTree SAEnv
evalCondStmtIf env (CondStmtIf expr bodyList) = do evalExprTypes env expr
                                                   evalBodyList env bodyList


evalCondStmtElifList :: SAEnv -> [CondStmtElif] -> EvalTree SAEnv
evalCondStmtElifList env [] = return $ env
evalCondStmtElifList env ((CondStmtElif expr bodyList) : xs) =
  do evalExprTypes env expr
     env'  <- evalBodyList env bodyList
     env'' <- updateNonFunctionalStmtOnClose env env'
     evalCondStmtElifList env'' xs


evalCondStmtElse :: SAEnv -> CondStmtElse -> EvalTree SAEnv
evalCondStmtElse env (CondStmtElse bodyList) = evalBodyList env bodyList



-- *********************** --
-- *                     * --
-- * Foreach Loop Stmt's * --
-- *                     * --
-- *********************** --
evalForeachLoopStmt :: SAEnv -> Ident -> Ident -> [GenericStmtBody] -> EvalTree SAEnv
evalForeachLoopStmt env (Ident iter) (Ident collection) bodyList =
  -- Iterable checks should be made in valid interpreter instance.
  -- The reason is that we cannot determine enough infos from dynamically changing
  -- program environment in just one go through the parse tree.
  do evalReadOnlyIdentVarTypes env collection
     let env' = addVariableToEnv env iter
     env'' <- evalBodyList env' bodyList
     updateNonFunctionalStmtOnClose env env''



-- ********************* --
-- *                   * --
-- * While Loop Stmt's * --
-- *                   * --
-- ********************* --
evalWhileLoopStmt :: SAEnv -> Expr -> [GenericStmtBody] -> EvalTree SAEnv
evalWhileLoopStmt env expr bodyList =
  do evalExprTypes env expr
     env' <- evalBodyList env bodyList
     updateNonFunctionalStmtOnClose env env'



-- ********************* --
-- *                   * --
-- * Structure Op Stmt * --
-- *                   * --
-- ********************* --
evalStructureOpStmt :: SAEnv -> StructureOpStmt -> EvalTree SAEnv
evalStructureOpStmt env structOp =
  case structOp of
    (DictStructureOpStmt dictOpStmt)   -> evalDictOpStmt env dictOpStmt
    (ListStructureOpStmt listOpStmt)   -> evalListOpStmt env listOpStmt
    (TupleStructureOpStmt tupleOpStmt) -> evalTupleOpStmt env tupleOpStmt


evalDictOpStmt :: SAEnv -> DictOpStmt -> EvalTree SAEnv
evalDictOpStmt env dictOpStmt =
  do case dictOpStmt of
       (DictUnionStmt (Ident ident) expr)           ->
          do evalReadNWriteIdentVarTypes env ident
             evExprTypes <- evalExprTypes env expr
             verifyDictionaryStructureIndexType evExprTypes ident "Dict: .union(E)"
       (DictRemoveElemByKeyStmt (Ident ident) expr) ->
          do evalReadNWriteIdentVarTypes env ident 
             evExprTypes <- evalExprTypes env expr
             verifyLiteralStructureIndexType evExprTypes ident "Dict: .remove(E)"
     return $ env


evalListOpStmt :: SAEnv -> ListOpStmt -> EvalTree SAEnv
evalListOpStmt env listOpStmt =
  do case listOpStmt of
       (ListPushElemStmt (Ident ident) expr) -> do evalReadNWriteIdentVarTypes env ident
                                                   evalExprTypes env expr
                                                   return $ env


evalTupleOpStmt :: SAEnv -> TupleOpStmt -> EvalTree SAEnv
evalTupleOpStmt env tupleOpStmt =
  do case tupleOpStmt of
       (TupleReplaceElemAtPosStmt (Ident ident) expr expr') ->
          do evalReadNWriteIdentVarTypes env ident
             evExprTypes <- evalExprTypes env expr
             verifyIntegerStructureIndexType evExprTypes ident "Tuple: replaceAt(E, )"
             evalExprTypes env expr'
             -- Any kind of evaluated and valid type fits replacement rules.
             -- That's why no other checks are required there!
     return $ env



-- ****************** --
-- *                * --
-- * Func Expr Stmt * --
-- *                * --
-- ****************** --
evalFuncExpr :: SAEnv -> FuncExpr -> EvalTree Result
evalFuncExpr env funcExpr =
  case funcExpr of
    (FuncExprKeywordArgs (FuncStmtDeclIdent (Ident funName)) keywordArgs) ->
       do safDesc <- extractFunDescEnv env funName
          evalFuncExprKeywordArgs env (Set.empty) keywordArgs funName
    (FuncExprPositionalArgs (FuncStmtDeclIdent (Ident funName)) positionalArgs) ->
       do safDesc <- extractFunDescEnv env funName
          let usedParams     = length positionalArgs
              expectedParams = length (argsOrd safDesc)
          if ((/=) usedParams expectedParams) then
             throwError $ invalidNumberOfPositionalArgsError funName expectedParams usedParams
             else
                evalFuncExprPositionalArgs env positionalArgs funName (argsOrd safDesc)


extractFunDescEnv :: SAEnv -> FunName -> EvalTree SAFDesc
extractFunDescEnv env funName =
  case Map.lookup funName (funEnv env) of
    Nothing      -> throwError $ undefinedFunctionError funName
    Just safDesc -> return $ safDesc


-------------------------------
-- Func Expr Positional Args --
-------------------------------
evalFuncExprPositionalArgs :: SAEnv -> [PositionalArg] -> FunName -> [VarName] -> EvalTree Result
evalFuncExprPositionalArgs env [] _ _ = return $ ()
evalFuncExprPositionalArgs env ((PositionalArg x) : xs) funName (y : ys) =
  do evalExprTypes env x
     evalFuncExprPositionalArgs env xs funName ys


-- `ExprTypes` return type is required due to usage of methods
-- that are normally involved in epxression evaluation process.
{-getPositionalVarType :: SAEnv -> PositionalArg -> FunName -> EvalTree ExprTypes
getPositionalVarType env positionalArg funName =
  case positionalArg of
    LiteralArg literal          -> evalLiteralVarType literal
    StructureArg structure      -> evalStructureType env structure
    VariableArg (Ident varName) -> evalReadOnlyIdentVarTypes env varName-}


----------------------------
-- Func Expr Keyword Args --
----------------------------
evalFuncExprKeywordArgs :: SAEnv -> VarSet -> [KeywordArg] -> FunName -> EvalTree ()
-- Case all keyword args processed
evalFuncExprKeywordArgs env varSet [] funName =
  do let safDesc          = fromJust $ Map.lookup funName $ funEnv env
         defArgsSet       = defArgs safDesc
         argsSet          = args safDesc
         argsUnion        = Set.union defArgsSet argsSet
         argsDifference   = Set.difference argsUnion varSet
         defIntersection  = Set.intersection argsDifference defArgsSet
         errorList        = Set.toList $ Set.difference defIntersection argsDifference
     if (/=) defIntersection argsDifference then
        throwError $ unspecifiedKeywordArgsError errorList funName
        else
           return $ ()


-- Case processing keywords
evalFuncExprKeywordArgs env varSet ((KeywordArg (Ident varName) expr) : xs) funName =
  do let safDesc = fromJust $ Map.lookup funName $ funEnv env
     checkValidKeywordVar safDesc varName funName
     evalExprTypes env expr
     evalFuncExprKeywordArgs env (Set.insert varName varSet) xs funName


checkValidKeywordVar :: SAFDesc -> VarName -> FunName -> EvalTree Result
checkValidKeywordVar safDesc varName funName =
  do let argsUnionSet = Set.union (defArgs safDesc) (args safDesc)
     if not $ Set.member varName argsUnionSet then
        throwError $ invalidKeywordArgError varName funName
        else 
           return $ ()

      
    
-- ************** --
-- *            * --
-- * Eval Ident * --
-- *            * --
-- ************** --
evalReadOnlyIdentVarTypes :: SAEnv -> VarName -> EvalTree ExprTypes
evalReadOnlyIdentVarTypes env varName =
  do let varEnvsUnionSet         = Set.union (globalVarEnv env) (localVarEnv env)
     if Set.member varName varEnvsUnionSet then
        return $ getAllVarTypes
        else
           throwError $ undefinedVarInExprError varName


evalReadNWriteIdentVarTypes :: SAEnv -> VarName -> EvalTree ExprTypes
evalReadNWriteIdentVarTypes env varName =
  do if (==) (scope env) Global then
        do if Set.member varName (globalVarEnv env) then
              return $ getAllVarTypes
              else
                 throwError $ undefinedVarInExprError varName
        else
           do let availableVarNames = Set.union (localVarEnv env) (globalVarInUse env)
              if Set.member varName availableVarNames then
                 return $ getAllVarTypes
                 else
                    throwError $ undefinedVarInExprError varName



-- ************* --
-- *           * --
-- * Eval Expr * --
-- *           * --
-- ************* --
evalExprTypes :: SAEnv -> Expr -> EvalTree ExprTypes
evalExprTypes env expr =
  case expr of
    -- ****** --
    -- <Expr> --
    -- ****** --
    (ExprAtom atom)                   -> evalAtomTypes env atom
    
    -- The type of logic operation is always [BoolT].
    -- Each single type is required to be easily converted into BoolT.
    -- Hence, only after operands evalute to legal type, mapping should be
    -- applied so that the final result should also evaluate to [BoolT].
    (ExprAlternativeOp expr expr')    -> evalTwoArgLogicOpType env expr expr'
    (ExprConjunctionOp expr expr')    -> evalTwoArgLogicOpType env expr expr'
    (ExprNegationOp expr)             -> evalOneArgLogicOpType env expr

    -- Unlike logic operation group, others like `arithmetics` and
    -- `comparisons` require that both operands have the same type.
    (ExprNotEqualOp expr expr')       -> evalTwoArgStronglyTypedOpType env expr expr'
    (ExprEqualOp expr expr')          -> evalTwoArgStronglyTypedOpType env expr expr'
    (ExprGreaterEqualOp expr expr')   -> evalTwoArgStronglyTypedOpType env expr expr'
    (ExprGreaterOp expr expr')        -> evalTwoArgStronglyTypedOpType env expr expr'
    (ExprLesserEqualOp expr expr')    -> evalTwoArgStronglyTypedOpType env expr expr'
    (ExprLesserOp expr expr')         -> evalTwoArgStronglyTypedOpType env expr expr'

    (ExprAdditionOp expr expr')       -> evalTwoArgStronglyTypedOpType env expr expr'
    (ExprSubtractionOp expr expr')    -> evalTwoArgStronglyTypedOpType env expr expr'
    (ExprMultiplicationOp expr expr') -> evalTwoArgStronglyTypedOpType env expr expr'
    (ExprDivisionOp expr expr')       -> evalTwoArgStronglyTypedOpType env expr expr'


---------------------
-- Atom Operations --
---------------------
evalAtomTypes :: SAEnv -> Atom -> EvalTree ExprTypes
evalAtomTypes env atom =
  case atom of
    (AtomIdent (Ident varName))   -> evalReadOnlyIdentVarTypes env varName
    (AtomLiteral literal)         -> evalLiteralVarType literal
    (AtomFuncExpr funcExpr)       -> evalFuncExprTypes env funcExpr
    (AtomStructure structure)     -> evalStructureType env structure
    (AtomStructureOp structureOp) -> evalStructureOpType env structureOp


evalLiteralVarType :: Literal -> EvalTree ExprTypes
evalLiteralVarType literal =
  case literal of
    (BoolLiteral _)    -> return $ [BoolT]
    (IntegerLiteral _) -> return $ [IntegerT]


evalFuncExprTypes :: SAEnv -> FuncExpr -> EvalTree ExprTypes
evalFuncExprTypes env funcExpr =
  -- Parse func expression 
  do evalFuncExpr env funcExpr
     case funcExpr of
       (FuncExprKeywordArgs (FuncStmtDeclIdent (Ident funName)) _)    ->
          evalFuncExprTypes' env funName
       (FuncExprPositionalArgs (FuncStmtDeclIdent (Ident funName)) _) ->
          evalFuncExprTypes' env funName


evalFuncExprTypes' :: SAEnv -> FunName -> EvalTree ExprTypes
evalFuncExprTypes' env funName =
  case Map.lookup funName $ funEnv env of
    Just safDesc -> do let returnT' = Set.toList $ returnT safDesc
                       if not $ null $ returnT' then
                          return $ returnT'
                          else
                             throwError $ procedureReturnError funName
    Nothing      -> throwError $ undefinedFunctionError funName


evalStructureType :: SAEnv -> Structure -> EvalTree ExprTypes
evalStructureType env structure =
  case structure of
    -- Structure evaluation is necessary because it can contain other expressions inside.
    (MutableStructure mutable)     ->
       case mutable of
         (MutableStructureList list)     -> evalListStructureType env list
         (MutableStructureDict dict)     -> evalDictStructureType env dict
    (ImmutableStructure immutable) ->
       case immutable of
         (ImmutableStructureTuple tuple) -> evalTupleStructureType env tuple


evalStructureOpType :: SAEnv -> StructureOp -> EvalTree ExprTypes
evalStructureOpType env structureOp =
  do case structureOp of
       ----------
       -- List --
       ----------
       (ListGetElemByPos (Ident ident) expr)     -> do evalReadOnlyIdentVarTypes env ident
                                                       evExprTypes  <- evalExprTypes env expr
                                                       verifyIntegerStructureIndexType evExprTypes ident "List: [E]"
       (ListGetSublist (Ident ident) expr expr') -> do evalReadOnlyIdentVarTypes env ident
                                                       evExprTypes  <- evalExprTypes env expr
                                                       verifyIntegerStructureIndexType evExprTypes ident "List: [E : ]"
                                                       evExprTypes' <- evalExprTypes env expr'
                                                       verifyIntegerStructureIndexType evExprTypes' ident "List: [ : E]"
       (ListGetLhsSublist (Ident ident) expr)    -> do evalReadOnlyIdentVarTypes env ident
                                                       evExprTypes  <- evalExprTypes env expr
                                                       verifyIntegerStructureIndexType evExprTypes ident "List: [E : ]"
       (ListGetRhsSublist (Ident ident) expr)    -> do evalReadOnlyIdentVarTypes env ident
                                                       evExprTypes  <- evalExprTypes env expr
                                                       verifyIntegerStructureIndexType evExprTypes ident "List: [ : E]"
       (ListGetFrontElem (Ident ident))          -> do evalReadOnlyIdentVarTypes env ident
                                                       return $ ()
       (ListGetLastElem (Ident ident))           -> do evalReadOnlyIdentVarTypes env ident
                                                       return $ ()
       (ListGetLength (Ident ident))             -> do evalReadOnlyIdentVarTypes env ident
                                                       return $ ()
       (ListPopFrontElem (Ident ident))          -> do evalReadNWriteIdentVarTypes env ident
                                                       return $ ()
       (ListPopLastElem (Ident ident))           -> do evalReadNWriteIdentVarTypes env ident
                                                       return $ ()

       ----------
       -- Dict --
       ----------
       (DictGetElemByKey (Ident ident) expr)     -> do evalReadOnlyIdentVarTypes env ident
                                                       evExprTypes  <- evalExprTypes env expr
                                                       verifyLiteralStructureIndexType evExprTypes ident "Dict: .get(E)"
       (DictGetKey (Ident ident))                -> do evalReadOnlyIdentVarTypes env ident
                                                       return $ ()
       (DictGetValue (Ident ident))              -> do evalReadOnlyIdentVarTypes env ident
                                                       return $ ()
       (DictPopElemByKey (Ident ident) expr)     -> do evalReadNWriteIdentVarTypes env ident
                                                       evExprTypes  <- evalExprTypes env expr
                                                       verifyLiteralStructureIndexType evExprTypes ident "Dict: .pop(E)"

       -----------
       -- Tuple --
       -----------
       (TupleGetElemAtPos (Ident ident) expr)    -> do evalReadOnlyIdentVarTypes env ident
                                                       evExprTypes  <- evalExprTypes env expr
                                                       verifyIntegerStructureIndexType evExprTypes ident "Tuple: .at(E)"
       (TupleGetSize (Ident ident))              -> do evalReadOnlyIdentVarTypes env ident
                                                       return $ ()
     return $ getAllVarTypes


evalOneExprStructureOp :: VarTypes -> VarTypes -> VarName -> String -> EvalTree Result
evalOneExprStructureOp evExprTypes allowedExprTypes varName opErrorMsg =
  if null $ List.intersect evExprTypes allowedExprTypes then
     throwError $ unsuitableExprTypeForStructureOpError varName opErrorMsg evExprTypes allowedExprTypes
     else
        return $ ()


verifyIntegerStructureIndexType :: VarTypes -> VarName -> String -> EvalTree Result
verifyIntegerStructureIndexType varTypes varName opErrorMsg =
  evalOneExprStructureOp varTypes [IntegerT] varName opErrorMsg


verifyLiteralStructureIndexType :: VarTypes -> VarName -> String -> EvalTree Result
verifyLiteralStructureIndexType varTypes varName opErrorMsg =
  evalOneExprStructureOp varTypes [BoolT, IntegerT] varName opErrorMsg


verifyDictionaryStructureIndexType :: VarTypes -> VarName -> String -> EvalTree Result
verifyDictionaryStructureIndexType varTypes varName opErrorMsg =
  evalOneExprStructureOp varTypes [MutableDictT] varName opErrorMsg



-- List --
evalListStructureType :: SAEnv -> List -> EvalTree ExprTypes
evalListStructureType env (List []) = return $ [MutableListT]
evalListStructureType env (List ((ListExpr x) : xs)) = do evalExprTypes env x
                                                          evalListStructureType env (List xs)

-- Dict --
evalDictStructureType :: SAEnv -> Dict -> EvalTree ExprTypes
evalDictStructureType env (Dict []) = return $ [MutableDictT]
evalDictStructureType env (Dict (x : xs)) =
  case x of
    (DictKeyDatumIdent (Ident varName) expr) -> do evalReadOnlyIdentVarTypes env varName
                                                   evalExprTypes env expr
                                                   evalDictStructureType env (Dict xs)
    (DictKeyDatumLiteral _ expr)             -> do evalExprTypes env expr
                                                   evalDictStructureType env (Dict xs)


-- Tuple --
evalTupleStructureType :: SAEnv -> Tuple -> EvalTree ExprTypes
evalTupleStructureType env (Tuple []) = return $ [ImmutableTupleT]
evalTupleStructureType env (Tuple ((TupleExpr x) : xs)) = do evalExprTypes env x
                                                             evalTupleStructureType env (Tuple xs)



----------------------
-- Logic Operations --
----------------------
evalOneArgLogicOpType :: SAEnv -> Expr -> EvalTree ExprTypes
evalOneArgLogicOpType env expr = do evalExprTypes env expr
                                    return $ [BoolT]


evalTwoArgLogicOpType :: SAEnv -> Expr -> Expr -> EvalTree ExprTypes
evalTwoArgLogicOpType env expr expr' = do evalExprTypes env expr
                                          evalExprTypes env expr'
                                          return $ [BoolT]


-------------------------------
-- Strongly Typed Operations --
-------------------------------
checkEqualExprTypes :: ExprTypes -> ExprTypes -> EvalTree ExprTypes
checkEqualExprTypes exprTypes exprTypes' =
  let exprTypesIntersection = List.intersect exprTypes exprTypes'
  in if not $ null exprTypesIntersection then
        return $ exprTypesIntersection
        else
           throwError $ divergentExprTypesError exprTypes exprTypes'


evalTwoArgStronglyTypedOpType :: SAEnv -> Expr -> Expr -> EvalTree ExprTypes
evalTwoArgStronglyTypedOpType env expr expr' =
  do exprTypes  <- evalExprTypes env expr
     exprTypes' <- evalExprTypes env expr'
     checkEqualExprTypes exprTypes exprTypes'

