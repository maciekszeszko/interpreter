module InterpreterMessages where

import DataTypes

errorHeader   = "Runtime ERROR: "
warningHEader = "Runtime WARNING: "


-- ********** --
-- *        * --
-- * Errors * --
-- *        * --
-- ********** --
functionNoReturnValueError funName              = errorHeader ++ "function `" ++ funName ++ "` is supposed to return a value."
functionReturnValueError funName                = errorHeader ++ "function `" ++ funName ++ "` should not return a value."
notIterableCollectionError collName             = errorHeader ++ "`" ++ collName ++ "` is not an iterable collection."
notAssignableListCollectionError varName        = errorHeader ++ "variable `" ++ varName ++ "` is not an assignable list collection."
notIntegerIndexInCollectionError idx varName    = errorHeader ++ "index " ++ (show idx) ++ " specified for collection `" ++ varName ++
                                                                 "` is not an IntegerObject."
undefinedPushOperationError varName             = errorHeader ++ "`push` operation is undefined for collection `" ++ varName ++ "`."
invalidDictUnionOperandError varName            = errorHeader ++ "`union` operation is undefined for collection `" ++ varName ++ "`."
invalidDictUnionExpressionError value           = errorHeader ++ "cannot perform dict `union` operation for expression " ++ (show value) ++ "."
invalidTupleReplaceAtOperandError varName       = errorHeader ++ "`replaceAt` operation is undefined for collection `" ++ varName ++ "`."
invalidKeyTypeForDictError value                = errorHeader ++ "invalid key type for dictionary in value " ++ (show value) ++ "."
boolKeyDoesNotExistError bKey                   = errorHeader ++ "Bool key: " ++ (show bKey) ++ " does not exist."
integerKeyDoesNotExistError iKey                = errorHeader ++ "Integer key: " ++ (show iKey) ++ " does not exist."
invalidSublistRangeError fromR toR              = errorHeader ++ "invalid sublist range for " ++ (show fromR) ++ ", " ++ (show toR) ++ "."
indexOutOfBoundsError varName toR               = errorHeader ++ "index " ++ (show toR) ++ " out of bounds for list `" ++ varName ++ "."
sublistBoundsNotIntegersError fromR toR         = errorHeader ++ "sublist bounds: " ++ (show fromR) ++ ", " ++ (show toR) ++
                                                                 " are supposed to be of IntegerObject type."
notListCollectionError varName                  = errorHeader ++ "variable `" ++ varName ++ " does not evaluate to the ListObject."
lengthOnNonListCollectionError varName          = errorHeader ++ "cannot access `length` attribute on variable `" ++ varName ++
                                                                 " that does not evaluate to ListObject type."
popFrontOnNonListCollectionError varName        = errorHeader ++ "cannot invoke `popFront` method on variable `" ++ varName ++
                                                                 " that does not evaluate to ListObject type."
popLastOnNonListCollectionError varName         = errorHeader ++ "cannot invoke `popLast` method on variable `" ++ varName ++
                                                                 " that does not evaluate to ListObject type."
invalidDictObjectError varName                  = errorHeader ++ "variable `" ++ varName ++ " does not evaluate to DictObject type."
invalidTupleObjectError varName                 = errorHeader ++ "variable `" ++ varName ++ " does not evaluate to TupleObject tyape."
sizeOnNonTupleCollectionError varName           = errorHeader ++ "cannot access `size` attribute on variable `" ++ varName ++
                                                                 " that does not evaluate to TupleObject type."
invalidComparisonOperandsError lhsV rhsV        = errorHeader ++ "cannot compare two different types by implicit boolean cast\n" ++
                                                                 "LHS operand: " ++ (show lhsV) ++ "\nRHS operand: " ++ (show rhsV)
invalidAdditionOperationOperandsError lhsV rhsV = errorHeader ++ "`+` operation is only defined for types [IntegerObject, DictObject, ListObject]\n" ++
                                                                 "LHS operand: " ++ (show lhsV) ++ "\nRHS operand: " ++ (show rhsV)
arithmeticOperationOperandsError op lhsV rhsV   = errorHeader ++ "`" ++ op ++ "` is only defined for IntegerObject type\n" ++
                                                                 "LHS operand: " ++ (show lhsV) ++ "\nRHS operand: " ++ (show rhsV)
divisionByZeroError                             = errorHeader ++ "Division by zero is strictly forbidden."
notSingletonDictInstanceError varName           = errorHeader ++ "Dictionary `" ++ varName ++ "` is not a singleton instance."

