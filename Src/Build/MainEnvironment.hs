module MainEnvironment where

import qualified Data.Map as Map
import qualified Data.Set as Set

import AbsGrammar

import DataTypes


type FunName  = String
type VarName  = String
type VarNames = [VarName]

type FunScopeSet = Set.Set FunName
type VarMap      = Map.Map VarName ValueT
type VarSet      = Set.Set VarName

type DefArgMapper = Map.Map VarName Expr

type FunState = Maybe FunName

data ReturnExprState = UNHANDLED
                     | NO_RETURN
                      deriving (Show)

data Scope       = Global
                 | Local
                  deriving (Eq, Show)

----------------------
-- Main Environment --
----------------------
data MainEnv    = MEnv {
                    currFunName :: FunState,
                    funEnv      :: FunEnv,
                    varEnv      :: VarEnv,
                    retEnv      :: ReturnEnv,
                    scope       :: Scope
                  }


--------------------------
-- Function Environment --
--------------------------
data FunEnv     = FEnv {
                    funDesc  :: Map.Map FunName FunEnvDesc
                    -- funScope :: FunScopeSet
                  }


data FunEnvDesc = FEDesc {
                    funExecute :: FunExecute,
                    funParams  :: FunParams,
                    varScope   :: VarScope
                  }

data FunExecute = FExecute {
                    bodyCode     :: [GenericStmtBody],
                    defArgMapper :: DefArgMapper
                  }

data FunParams  = FParams {
                    defaultSet    :: VarSet,
                    defaultMap    :: VarMap,
                    orderList     :: VarNames,
                    positionalSet :: VarSet
                  }

data ReturnEnv     = REnv {
                    state :: ReturnExprState,
                    value :: ValueT
                  }

data VarScope   = VScope {
                    globalVariablesVScope      :: VarSet,
 --                   globalVariablesInUseVScope :: VarSet,
                    localVariablesVScope       :: VarSet
                  } deriving (Show, Eq)



--------------------------
-- Variable Environment --
--------------------------
data VarEnv = VEnv {
                globalVariablesVEnv      :: VarMap,
                globalVariablesInUseVEnv :: VarSet,
                localVariablesVEnv       :: VarMap
              }

