MyPython   
=========


Intro:  
------
`MyPython` is dynamically typed language with pretty rich support to multiple data structures.  
It's main purpose is to perform basic computations like graph searches or other academic stuff  
that does not require support of external high level programming language libraries.  


Brief list of features:  
-----------------------
  * basic primitive types: `Integer`, `Bool`,  
  * arithmetics (`+` operation has been also overloaded for dicts and lists),  
  * comparisons (**well-defined** only for operands of the **same type**),  
  * logic operations (each type has built-in mapping to `Bool` type),  
  * loops `while` and `foreach`, where the latter is **immutable** during it's traversal,  
  * conditions: `if-elif-else` - optionally `elif` and `else`,  
  * support for both functions and procedures, also `nested` definitions are allowed,  
  * print command, eg. **print a, {b: [3, true, {k: [3, 1, {false: true}]}]}, j**,  
  * passing parameter by value and by variable copy,  
  * global and local variables, scopes,  
  * both dynamic and static error handling,  
  * structures: `list`, `tuple`, `dictionary`, where each of which is capable of storing all defined types,  
  * **UP**: convenient *API* for structures mentioned above,  
  * tuple assignment inherited from *Python*, eg. **a, b, d = k, fun(m=16, l=18), 8**,  
  * passing arguments both positionally or by keywords (in case of lack the latter
    one default arg value prescribed by programmer is used)



API: 
====
  * **list**:  

    1. a[index]:  
       * return value: specified elem,  
       * action: no action 
    2. a[index] = x:  
       * return value: no return,  
       * action: update of concrete elem  
    3. a[lhs:rhs]:  
       * return value: list from range [lhs, rhs],  
       * action: no action  
    4. a[:rhs]:  
       * return value: list from range [first_available_item, rhs],  
       * action: no action  
    5. a[lhs:]:  
       * return value: list from range [lhs, last_available_item],  
       * action: no action  
    6. a.push(expr):   
       * return value: no return,  
       * action: list with appended elem  
    7. a.popFront():  
       * return value: front elem on list,  
       * action: list without that elem   
    8. a.popLast():  
       * return value: last elem on list,    
       * action: list without that elem   
    9. a.length:  
       * return value: list length,  
       * action: no action  
   10. a.front():  
       * return value: front elem on list,  
       * action: no action  
   11. a.last():  
       * return value: last elem on list,  
       * action: no action  


  * **dict**:  

    1. d.get(expr): 
       * return value: value of singleton dict instance,  
       * action: no action  
    2. d.key():  
       * return value: key of singleton dict instance,  
       * action: no action  
    3. d.value():  
       * return value: value of singleton dict instance,  
       * action: no action  
    4. d.pop(expr):  
       * return value: key with evaluated expression,  
       * action: dict without that key  
    5. d.union(expr):  
       * return value: no return,  
       * action: union dict with dict evaluated expression  
    6. d.remove(expr):  
       * return value: no return,  
       * action: remove key from dict  


  * **tuple**:  

    1. t.at(expr):  
       * return value: elem,  
       * action: no action  
    2. t.replaceAt(exprIndex, exprValue):  
       * return value: no return,  
       * action: replace element with expression evaluated value  
    3. t.size:  
       * return value: int,  
       * action: no action  


### All error messages displayed during runtime can be found in `InterpreterMessages` module, `Build` dir.



Few words about static analyzer:  
--------------------------------  
Despite dynamic binding in chosen language (similarity with Python),  
I decided to give it as much support on static level as possible.  
These are only main functionalities that are checked there:  
  * ambiguous arguments in function declaration / function expression,  
  * both single and multi assignment syntax correctness,  
  * divergent of expression types,  
  * invalid global identifier reference,    
  * invalid number of positional args,  
  * procedure evaluated as expression ( .. unexpected behaviour ofc => error .. ),  
  * redeclaration of global variable (WARNING),  
  * return statement in global scope (what could terminating statement in global scope mean?),  
  * undefined functions,  
  * undefined variables in expressions,  
  * unspecified keyword arguments  




What I thought I would like to include and was a bad idea:  
----------------------------------------------------------   
Definately and for sure the most ambicious and the worst idea during design and implementation of static analyzer  
was the concept of puting constraints on function arguments based on it's context operations, like expressions.  
It probably could be solved if the language was statically typed or basically didn't support nested functions.  
Simple algorithm that could win this problem for us would be DFS with an accumulator that remembers  
Set of variable names and a list of still available types during evaluating expressions.  
However, if we do not have statically typed language, variables can take any valid type during runtime.  
What is more, if we support nested functions then in static analysis, where we actually do not want to evaluate  
program but rather perform basic checks with scanning parse tree only once, we cannot determine the order  
of function calls that would modify program environment. So it turns out that this problems requires definately  
more effort, and even should not be considered as a task for static analysis stage.  


Usage (of course..)  
-------------------  
a) Jump into Src/Build dir.  
b) Run script `startup.sh` and wait 5 sec until it produces You `Interpreter` executable with all required submodules.  
c) Run executable with filePath like: ./Interpreter [filePath]


Examples  
--------  
Additionaly bunch of test files has been attached in the `Example` directory.  
Those examples contain both `Good` scenarios and `Bad` scenarios with full error coverage  
for `StaticAnalyzer` and with just few error-examples that may occur in `Runtime`.



Useful links:
=============  
  * The [BNF Converter page](http://bnfc.digitalgrammars.com/)  
  * The [Labelled BNF Grammar Formalism](http://bnfc.digitalgrammars.com/LBNF-report.pdf)

 
